package com.dfilatov.istore.exception;

/**
 * Using for stop program
 * after critical errors.
 *
 * @author Denys Filatov
 */
public class IStoreException extends RuntimeException {

    private static final long serialVersionUID = 2680851502170961205L;

    public IStoreException(String message, Throwable cause) {
        super(message, cause);
    }
}
