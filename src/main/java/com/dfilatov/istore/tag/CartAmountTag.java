package com.dfilatov.istore.tag;

import com.dfilatov.istore.model.Cart;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * JSTL tag class for showing User Shopping Cart's
 * amount and total cost.
 *
 * @author Denys Filatov
 */
public class CartAmountTag extends TagSupport {
    private static final long serialVersionUID = 3552676744734792555L;
    private Cart cart;

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    @Override
    public int doStartTag() throws JspException {
        JspWriter out = pageContext.getOut();
        try {
            out.println("<th class=\"text-center\" scope=\"col\">Total cost: " +
                    cart.getTotalCost() + "</th>\n" +
                    "<th class=\"text-center\" scope=\"col\">Total count: " +
                    cart.getItems().size() + "</th>\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return SKIP_BODY;
    }
}
