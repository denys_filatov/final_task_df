package com.dfilatov.istore.db;

import com.dfilatov.istore.db.entity.AbstractEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * Update count of entity items
 * @param <T>
 */
public interface CountSynchronizable<T extends AbstractEntity> {

    void updateElementsCount(List<T> elements);

    void updateSingleElementCount(T element);

    default void updateCount(int id, String sql) {
        PreparedStatement statement;
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            statement.setInt(2, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            if (connection != null) {
                DBManager.getInstance().rollbackAndClose(connection);
            }
        } finally {
            if (connection != null) {
                DBManager.getInstance().commitAndClose(connection);
            }
        }
    }
}
