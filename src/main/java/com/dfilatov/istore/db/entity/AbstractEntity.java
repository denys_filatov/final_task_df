package com.dfilatov.istore.db.entity;

import java.io.Serializable;

/**
 * Abstract class for all entities.
 *
 * @param <T> Integer or Long
 *
 * @author Denys Filatov
 */

public abstract class AbstractEntity<T> implements Serializable {
    private static final long serialVersionUID = -6754888866394775553L;

    private T id;

    public T getId() {
        return id;
    }

    public void setId(T id) {
        this.id = id;
    }
}
