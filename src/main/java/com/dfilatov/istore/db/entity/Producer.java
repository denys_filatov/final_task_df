package com.dfilatov.istore.db.entity;

import java.util.Objects;
import java.util.StringJoiner;

/**
 * Producer entity.
 *
 * @author Denys Filatov
 */
public class Producer extends AbstractEntity<Integer> {
    private static final long serialVersionUID = -2033629263419480298L;

    private String name;
    private int count;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Producer.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("productCount=" + count)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Producer producer = (Producer) o;
        return count == producer.count &&
                Objects.equals(name, producer.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, count);
    }
}
