package com.dfilatov.istore.db.entity;

import java.sql.Timestamp;
import java.util.StringJoiner;

/**
 * Ordering entity
 *
 * @author Denys Filatov
 */
public class Ordering extends AbstractEntity<Long> {
    private static final long serialVersionUID = 9029752890344510411L;

    private String account;
    private Timestamp created;
    private String orderStatus;


    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Ordering.class.getSimpleName() + "[", "]")
                .add("account=" + account)
                .add("created=" + created)
                .add("orderStatus=" + orderStatus)
                .toString();
    }
}
