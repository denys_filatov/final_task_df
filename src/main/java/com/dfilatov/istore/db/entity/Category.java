package com.dfilatov.istore.db.entity;

import java.util.Objects;
import java.util.StringJoiner;

/**
 * Category entity.
 *
 * @author Denys Filatov
 */
public class Category extends AbstractEntity<Integer> {

    private static final long serialVersionUID = 5412484266638220696L;

    private String name;

    private int count;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Category.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("count=" + count)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category = (Category) o;
        return count == category.count &&
                Objects.equals(name, category.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, count);
    }
}
