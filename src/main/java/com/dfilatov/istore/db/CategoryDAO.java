package com.dfilatov.istore.db;

import com.dfilatov.istore.db.entity.Category;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Category table DAO.
 *
 * @author Denys Filatov
 */
public class CategoryDAO implements AbstractDAO<Category>, CountSynchronizable<Category> {

    private static final String SQL_UPDATE_COUNT_OF_CATEGORY_ELEMENTS = "update category set category.product_count=" +
            "(SELECT sum(count) as productCount from product where product.id_category=?) where category.id=?;";

    private static final String SQL_DELETE_TRANSLATIONS_WHERE_CATEGORY_ID = "delete from translation where category_id=?";

    private static final String SQL_SELECT_TRANSLATIONS_WHERE_CATEGORY_ID = "SELECT t.category_name_translation as category FROM translation t where category_id=? order by t.language_id;";

    private static final String SQL_DELETE_CATEGORY_BY_ID = "delete from category c where c.id=?";

    private static final String SQL_UPDATE_TRANSLATIONS_BY_CATEGORY_ID = "update translation t set t.category_name_translation=? where t.category_id=? and t.language_id=?";

    private static final String SQL_FIND_ALL_CATEGORIES_WITHOUT_LOCALE = "select * from category;";

    private static final String SQL_FIND_ALL_CATEGORIES = "select c.id, t.category_name_translation as name, " +
            "c.product_count from category c " +
            "join translation t on c.id=t.category_id " +
            "join languages l on t.language_id=l.id group by c.id";
    private static final String SQL_FIND_ALL_CATEGORIES_WHERE_NAME = "select c.id, t.category_name_translation as name, " +
            "c.product_count from category c " +
            "join translation t on c.id=t.category_id " +
            "join languages l on t.language_id=l.id where l.name=?;";

    private static final String SQL_FIND_CATEGORY_BY_ID = "select * from category c where c.id = ?";

    private static final String SQL_UPDATE_CATEGORY_BY_ID = "update category c set c.name=?, c.product_count=? where c.id=?";
    private static final String SQL_INSERT_CATEGORY = "insert into category(name, product_count) VALUES (?,?)";
    private static final String SQL_INSERT_INTO_TRANSLATION = "insert into translation(category_name_translation, category_id, language_id) values(?,?,?);";
    private static final String SQL_FIND_ID_CATEGORY_BY_NAME = "select c.id from category c where c.name=?;";

    /**
     * Find category with localization
     *
     * @param locale locale name.
     * @return category.
     */
    public List<Category> findCategory(String locale) {
        List<Category> categoryList = new ArrayList<>();
        PreparedStatement statement;
        ResultSet resultSet;
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            CategoryMapper categoryMapper = new CategoryMapper();
            statement = connection.prepareStatement(SQL_FIND_ALL_CATEGORIES_WHERE_NAME);
            statement.setString(1, locale);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                categoryList.add(categoryMapper.mapRow(resultSet));
            }
        } catch (SQLException e) {
            if (connection != null) {
                DBManager.getInstance().rollbackAndClose(connection);
            }
        } finally {
            if (connection != null) {
                DBManager.getInstance().commitAndClose(connection);
            }
        }
        return categoryList;
    }

    /**
     * Find category translations by category id.
     *
     * @param id category id.
     * @return list of translations.
     */
    public List<String> findCategoryTranslationsById(Long id) {
        List<String> categoryTranslationList = new ArrayList<>();
        PreparedStatement statement;
        ResultSet resultSet;
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement(SQL_SELECT_TRANSLATIONS_WHERE_CATEGORY_ID);
            statement.setLong(1, id);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                categoryTranslationList.add(resultSet.getString("category"));
            }
        } catch (SQLException e) {
            if (connection != null) {
                DBManager.getInstance().rollbackAndClose(connection);
            }
        } finally {
            if (connection != null) {
                DBManager.getInstance().commitAndClose(connection);
            }
        }
        return categoryTranslationList;

    }

    /**
     * Get all categories with localization.
     *
     * @return list of categories.
     */
    @Override
    public List<Category> getAllElements() {
        return getAllElementsByQuery(new CategoryMapper(), SQL_FIND_ALL_CATEGORIES);
    }

    /**
     * Get all elements without localization.
     * Simpler query for add element method.
     *
     * @return list of categories.
     */
    public List<Category> getAllElementsWithoutLocalization() {
        return getAllElementsByQuery(new CategoryMapper(), SQL_FIND_ALL_CATEGORIES_WITHOUT_LOCALE);
    }

    @Override
    public Category findElementById(long id) {
        return findElementByIdAndQuery(new CategoryMapper(), SQL_FIND_CATEGORY_BY_ID, id);
    }

    @Override
    public void deleteElementById(long id) {
        changeById(id, SQL_DELETE_CATEGORY_BY_ID);
    }

    /**
     * Delete category translations before delete category.
     *
     * @param id category id.
     */
    public void deleteCategoryTranslationsById(long id) {
        changeById(id, SQL_DELETE_TRANSLATIONS_WHERE_CATEGORY_ID);
    }

    public int findCategoryIdByName(String name) {
        return findIdQuery(name, SQL_FIND_ID_CATEGORY_BY_NAME);
    }

    @Override
    public void updateElement(Category element) {
        PreparedStatement statement;
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement(SQL_UPDATE_CATEGORY_BY_ID);
            statement.setString(1, element.getName());
            statement.setInt(2, element.getCount());
            statement.setLong(3, element.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            if (connection != null) {
                DBManager.getInstance().rollbackAndClose(connection);
            }
        } finally {
            if (connection != null) {
                DBManager.getInstance().commitAndClose(connection);
            }
        }
    }

    /**
     * Add category translations after category was added.
     *
     * @param id           category id.
     * @param translations list of translations.
     */
    public void insertCategoryTranslations(int id, List<String> translations) {
        for (int i = 0; i < translations.size(); i++) {
            insertSingleCategoryTranslation(id, translations.get(i), i + 1);
        }
    }

    /**
     * Add category translation for single language.
     *
     * @param elementId   category id.
     * @param translation translation to update.
     * @param languageId  language id.
     */
    private void insertSingleCategoryTranslation(int elementId, String translation, int languageId) {
        PreparedStatement statement;
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement(SQL_INSERT_INTO_TRANSLATION);
            statement.setNString(1, translation);
            statement.setInt(2, elementId);
            statement.setInt(3, languageId);
            statement.executeUpdate();
        } catch (SQLException e) {
            if (connection != null) {
                DBManager.getInstance().rollbackAndClose(connection);
            }
        } finally {
            if (connection != null) {
                DBManager.getInstance().commitAndClose(connection);
            }
        }
    }

    @Override
    public void insertElement(Category element) {
        PreparedStatement statement;
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement(SQL_INSERT_CATEGORY);
            statement.setString(1, element.getName());
            statement.setInt(2, element.getCount());
            statement.executeUpdate();
        } catch (SQLException e) {
            if (connection != null) {
                DBManager.getInstance().rollbackAndClose(connection);
            }
        } finally {
            if (connection != null) {
                DBManager.getInstance().commitAndClose(connection);
            }
        }
    }

    /**
     * Update category translations where
     * category was updated.
     *
     * @param elementId    category id.
     * @param translations list of translations.
     */
    public void updateElementTranslations(Long elementId, List<String> translations) {
        for (int i = 0; i < translations.size(); i++) {
            int languageId = i + 1;
            updateSingleTranslation(elementId, translations.get(i), languageId);
        }
    }

    /**
     * Update category translation for single locale.
     *
     * @param elementId   category id.
     * @param translation translation to update.
     * @param languageId  language id.
     */
    public void updateSingleTranslation(Long elementId, String translation, int languageId) {
        translationUpdate(elementId, translation, languageId, SQL_UPDATE_TRANSLATIONS_BY_CATEGORY_ID);
    }

    /**
     * Update count of items in all categories.
     *
     * @param elements list of categories.
     */
    @Override
    public void updateElementsCount(List<Category> elements) {
        for (Category category : elements) {
            updateSingleElementCount(category);
        }
    }

    /**
     * Update one category's count of items.
     *
     * @param element category.
     */
    @Override
    public void updateSingleElementCount(Category element) {
        updateCount(element.getId(), SQL_UPDATE_COUNT_OF_CATEGORY_ELEMENTS);
    }

    /**
     * Mapper for category entity
     *
     * @author Denys Filatov
     */
    private static class CategoryMapper implements EntityMapper<Category> {
        @Override
        public Category mapRow(ResultSet rs) {
            try {
                Category category = new Category();
                category.setId(rs.getInt("id"));
                category.setName(rs.getString("name"));
                category.setCount(rs.getInt("product_count"));
                return category;
            } catch (SQLException throwables) {
                throw new IllegalStateException(throwables);
            }
        }
    }
}
