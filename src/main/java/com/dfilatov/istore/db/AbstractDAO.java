package com.dfilatov.istore.db;

import com.dfilatov.istore.db.entity.AbstractEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Abstract DAO class
 *
 * @param <T>
 * @author Denys Filatov
 */
public interface AbstractDAO<T extends AbstractEntity> {
    /**
     * Get list of all elements of entity.
     *
     * @return all elements of entity
     */
    List<T> getAllElements();

    /**
     * Find entity by id.
     *
     * @param id entity id.
     * @return element.
     */
    T findElementById(long id);

    /**
     * Delete element from database by id
     *
     * @param id element id.
     */
    void deleteElementById(long id);

    /**
     * Update element in database
     *
     * @param element entity to update.
     */
    void updateElement(T element);

    /**
     * Add new element to database
     *
     * @param element entity to insert.
     */
    void insertElement(T element);

    /**
     * Find element in database by id and
     * map it to entity.
     *
     * @param entityMapper entity mapper.
     * @param sql          sql query.
     * @param id           element id.
     * @return element.
     */
    default T findElementByIdAndQuery(EntityMapper<T> entityMapper, String sql, long id) {
        T element = null;
        PreparedStatement statement;
        ResultSet resultSet;
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement(sql);
            statement.setLong(1, id);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                element = entityMapper.mapRow(resultSet);
            }
        } catch (SQLException throwables) {
            if (connection != null) {
                DBManager.getInstance().rollbackAndClose(connection);
            }
        } finally {
            if (connection != null)
                DBManager.getInstance().commitAndClose(connection);
        }
        return element;

    }

    /**
     * Find all elements from table
     * using query.
     *
     * @param entityMapper entity mapper.
     * @param sql          sql query.
     * @return elements by query.
     */
    default List<T> getAllElementsByQuery(EntityMapper<T> entityMapper, String sql) {
        List<T> elementList = new ArrayList<>();
        PreparedStatement statement;
        ResultSet resultSet;
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                elementList.add(entityMapper.mapRow(resultSet));
            }
        } catch (SQLException e) {
            if (connection != null) {
                DBManager.getInstance().rollbackAndClose(connection);
            }
        } finally {
            if (connection != null) {
                DBManager.getInstance().commitAndClose(connection);
            }
        }
        return elementList;
    }

    /**
     * Do action on element by its id and query.
     *
     * @param id       element id.
     * @param sqlQuery sql query.
     */
    default void changeById(long id, String sqlQuery) {
        PreparedStatement statement;
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement(sqlQuery);
            statement.setLong(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            if (connection != null) {
                DBManager.getInstance().rollbackAndClose(connection);
            }
        } finally {
            if (connection != null) {
                DBManager.getInstance().commitAndClose(connection);
            }
        }
    }

    /**
     * Find element's id by its name.
     *
     * @param name  element name.
     * @param query sql query.
     * @return element's id.
     */
    default int findIdQuery(String name, String query) {
        int id = -1;
        PreparedStatement statement;
        ResultSet resultSet;
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement(query);
            statement.setNString(1, name);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                id = resultSet.getInt("id");
            }
        } catch (SQLException throwables) {
            if (connection != null) {
                DBManager.getInstance().rollbackAndClose(connection);
            }
        } finally {
            if (connection != null)
                DBManager.getInstance().commitAndClose(connection);
        }
        return id;
    }

    /**
     * Find count of elements in table by query
     *
     * @param id    element id.
     * @param query sql query.
     * @return count of elements.
     */
    default int findCount(long id, String query) {
        int result = -1;
        PreparedStatement statement;
        ResultSet resultSet;
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement(query);
            statement.setLong(1, id);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result = resultSet.getInt("count");
            }
        } catch (SQLException throwables) {
            if (connection != null) {
                DBManager.getInstance().rollbackAndClose(connection);
            }
        } finally {
            if (connection != null)
                DBManager.getInstance().commitAndClose(connection);
        }
        return result;
    }

    /**
     * Update translation table's elements.
     *
     * @param elementId                                   element's id.
     * @param translation                                 translation to update.
     * @param languageId                                  language's id.
     * @param sqlUpdateDescriptionTranslationsByProductId sql query.
     */
    default void translationUpdate(Long elementId, String translation, int languageId, String sqlUpdateDescriptionTranslationsByProductId) {
        PreparedStatement statement;
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement(sqlUpdateDescriptionTranslationsByProductId);
            statement.setNString(1, translation);
            statement.setLong(2, elementId);
            statement.setInt(3, languageId);
            statement.executeUpdate();
        } catch (SQLException e) {
            if (connection != null) {
                DBManager.getInstance().rollbackAndClose(connection);
            }
        } finally {
            if (connection != null) {
                DBManager.getInstance().commitAndClose(connection);
            }
        }
    }

}
