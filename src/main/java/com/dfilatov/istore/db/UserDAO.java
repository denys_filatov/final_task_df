package com.dfilatov.istore.db;

import com.dfilatov.istore.db.entity.Account;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * DAO for user entity.
 *
 * @author Denys Filatov
 */
public class UserDAO implements AbstractDAO<Account> {
    private static final String SQL_FIND_ALL_USERS = "select a.*, r.name as role " +
            "from account a, account_role r where r.id=a.id_role";
    private static final String SQL_FIND_ALL_ROLES = "select r.name as role from account_role r;";
    private static final String SQL_FIND_COUNT_OF_ALL_USER_ORDERS = "select count(*) as count from ordering o where o.id_account=?;";
    private static final String SQL_FIND_ACCOUNT_BY_LOGIN = SQL_FIND_ALL_USERS + " and a.name=?";
    private static final String SQL_FIND_USER_BY_ID = SQL_FIND_ALL_USERS + " and  a.id=?";
    private static final String SQL_DELETE_ACCOUNT_BY_ID = "delete from account a where a.id=?";
    private static final String SQL_UPDATE_ACCOUNT_BY_ID = "update account a set a.name=?, a.password=?, a.id_role=?, a.blocked=?, a.first_name=?, a.last_name=? where a.id=?";
    private static final String SQL_FIND_ACCOUNT_ROLE_BY_NAME = "select r.id as id from account_role r where r.name=?";
    private static final String SQL_INSERT_ACCOUNT = "insert into account(name, password, id_role, blocked, first_name, last_name) values(?,?,?,?,?,?)";


    @Override
    public Account findElementById(long id) {
        return findElementByIdAndQuery(new AccountMapper(), SQL_FIND_USER_BY_ID, id);
    }

    /**
     * Find account by it login.
     *
     * @param login account's login name.
     * @return account.
     */
    public Account findUserByLogin(String login) {
        Account account = new Account();
        PreparedStatement statement;
        ResultSet resultSet;
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            AccountMapper productMapper = new AccountMapper();
            statement = connection.prepareStatement(SQL_FIND_ACCOUNT_BY_LOGIN);
            statement.setString(1, login);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                account = productMapper.mapRow(resultSet);
            }
        } catch (SQLException throwables) {
            if (connection != null) {
                DBManager.getInstance().rollbackAndClose(connection);
            }
        } finally {
            if (connection != null)
                DBManager.getInstance().commitAndClose(connection);
        }
        return account;
    }

    /**
     * Find all user's orders by it id.
     *
     * @param id account id.
     * @return list of account orders.
     */
    public int findUserOrdersCountByID(Long id) {
        return findCount(id, SQL_FIND_COUNT_OF_ALL_USER_ORDERS);
    }

    /**
     * Find accountRole id by it's name.
     *
     * @param name account role name.
     * @return account role id.
     */
    public int findAccountRoleIdByName(String name) {
        return findIdQuery(name, SQL_FIND_ACCOUNT_ROLE_BY_NAME);
    }

    /**
     * Find all roles from database.
     *
     * @return list of account roles.
     */
    public List<String> findAllRoles() {
        List<String> roles = new ArrayList<>();
        PreparedStatement statement;
        ResultSet resultSet;
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement(SQL_FIND_ALL_ROLES);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                roles.add(resultSet.getString("role"));
            }
        } catch (SQLException throwables) {
            if (connection != null) {
                DBManager.getInstance().rollbackAndClose(connection);
            }
        } finally {
            if (connection != null)
                DBManager.getInstance().commitAndClose(connection);
        }
        return roles;

    }

    @Override
    public List<Account> getAllElements() {
        return getAllElementsByQuery(new AccountMapper(), SQL_FIND_ALL_USERS);
    }

    @Override
    public void deleteElementById(long id) {
        changeById(id, SQL_DELETE_ACCOUNT_BY_ID);
    }

    @Override
    public void updateElement(Account element) {
        PreparedStatement statement;
        Connection connection = null;
        int blocked;
        try {
            connection = DBManager.getInstance().getConnection();
            int roleID = findAccountRoleIdByName(element.getRole().toLowerCase());
            if (element.isBlocked()) {
                blocked = 1;
            } else {
                blocked = 0;
            }
            statement = connection.prepareStatement(SQL_UPDATE_ACCOUNT_BY_ID);
            statement.setString(1, element.getName());
            statement.setString(2, element.getPassword());
            statement.setInt(3, roleID);
            statement.setInt(4, blocked);
            statement.setNString(5, element.getFirstName());
            statement.setNString(6, element.getLastName());
            statement.setInt(7, element.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            if (connection != null) {
                DBManager.getInstance().rollbackAndClose(connection);
            }
        } finally {
            if (connection != null) {
                DBManager.getInstance().commitAndClose(connection);
            }
        }
    }

    @Override
    public void insertElement(Account element) {
        PreparedStatement statement;
        Connection connection = null;
        int blocked;
        try {
            connection = DBManager.getInstance().getConnection();
            int roleID = findAccountRoleIdByName(element.getRole().toLowerCase());
            if (element.isBlocked()) {
                blocked = 1;
            } else {
                blocked = 0;
            }
            statement = connection.prepareStatement(SQL_INSERT_ACCOUNT);
            statement.setString(1, element.getName());
            statement.setString(2, element.getPassword());
            statement.setInt(3, roleID);
            statement.setInt(4, blocked);
            statement.setNString(5, element.getFirstName());
            statement.setNString(6, element.getLastName());
            statement.executeUpdate();
        } catch (SQLException e) {
            if (connection != null) {
                DBManager.getInstance().rollbackAndClose(connection);
            }
        } finally {
            if (connection != null) {
                DBManager.getInstance().commitAndClose(connection);
            }
        }
    }

    /**
     * Mapper for account entity.
     *
     * @author Denys Filatov
     */
    private static class AccountMapper implements EntityMapper<Account> {
        @Override
        public Account mapRow(ResultSet rs) {
            try {
                Account account = new Account();
                account.setId(rs.getInt("id"));
                account.setName(rs.getString("name"));
                account.setPassword(rs.getString("password"));
                account.setRole(rs.getString("role"));
                account.setBlocked(rs.getBoolean("blocked"));
                account.setFirstName(rs.getNString("first_name"));
                account.setLastName(rs.getNString("last_name"));
                return account;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
