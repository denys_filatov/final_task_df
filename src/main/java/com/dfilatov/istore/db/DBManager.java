package com.dfilatov.istore.db;

import com.dfilatov.istore.exception.IStoreException;
import org.apache.commons.dbcp2.BasicDataSource;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 * DB manager. Works with MySQL.
 *
 * @author Denys Filatov
 */

public class DBManager {

    private final Properties applicationProperties = new Properties();

    private BasicDataSource dataSource;
    private static DBManager instance;

    public static synchronized DBManager getInstance() {
        if (instance == null)
            instance = new DBManager();
        return instance;
    }

    private DBManager() {
        loadApplicationProperties();
    }

    public Connection getConnection() throws SQLException {
        if (dataSource == null) {
            dataSource = new BasicDataSource();
        }
        dataSource.setDriverClassName(getApplicationProperty("db.driver"));
        dataSource.setUrl(getApplicationProperty("db.url"));
        dataSource.setUsername(getApplicationProperty("db.username"));
        dataSource.setPassword(getApplicationProperty("db.password"));
        dataSource.setInitialSize(Integer.parseInt(getApplicationProperty("db.pool.initSize")));
        dataSource.setMaxTotal(Integer.parseInt(getApplicationProperty("db.pool.maxSize")));
        dataSource.setDefaultAutoCommit(false);
        return dataSource.getConnection();
    }


    /**
     * Commits and close the given connection.
     *
     * @param con Connection to be committed and closed.
     */
    public void commitAndClose(Connection con) {
        try {
            con.commit();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Rollbacks and close the given connection.
     *
     * @param con Connection to be rollbacked and closed.
     */
    public void rollbackAndClose(Connection con) {
        try {
            con.rollback();
            con.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Get property from properties object.
     *
     * @param key property key's name.
     * @return property value.
     */
    public String getApplicationProperty(String key) {
        return applicationProperties.getProperty(key);
    }

    /**
     * Load application properties from application.properties file.
     */
    private void loadApplicationProperties() {
        try (InputStream inputStream = DBManager.class.getClassLoader().getResourceAsStream("application.properties")) {
            applicationProperties.load(inputStream);
        } catch (IOException e) {
            throw new IStoreException(e.getMessage(), e);
        }
    }

}
