package com.dfilatov.istore.db;

import com.dfilatov.istore.db.entity.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * DAO for product entity.
 *
 * @author Denys Filatov
 */
public class ProductsDAO implements AbstractDAO<Product> {
    private int noOfRecords;

    private static final String SQL_REMOVE_ALL_RESERVED = "update product set reserved=0 where id>0;";
    private static final String SQL_DELETE_RESERVE_FOR_SINGLE_PRODUCT = "update product set reserved=0 where id=?";
    private static final String SQL_SELECT_NAME_TRANSLATIONS_WHERE_CATEGORY_ID = "SELECT t.product_name_translation as name FROM translation t where product_id=? order by t.language_id;";
    private static final String SQL_SELECT_DESCRIPTIONS_TRANSLATIONS_WHERE_CATEGORY_ID = "SELECT t.product_description_translation as description FROM translation t where product_id=? order by t.language_id;";

    private static final String SQL_FIND_ALL_PRODUCTS_WHERE = "select SQL_CALC_FOUND_ROWS distinct p.id, " +
            "t.product_name_translation as name, t.product_description_translation as description, " +
            "p.image_link, p.price, p.reserved, cl.category_name_translation as category, " +
            "pr.name as producer, p.made_date as date, p.count as count " +
            "from product p " +
            "join category c on c.id=p.id_category " +
            "join producer pr on pr.id=p.id_producer " +
            "join translation t on p.id=t.product_id " +
            "join languages l on l.id=t.language_id " +
            "join " +
            "(select c.id, t.category_name_translation, l.name from category c " +
            "join translation t on t.category_id=c.id " +
            "join languages l on t.language_id=l.id " +
            "where l.name=?)" +
            "as cl on cl.id=c.id " +
            "where l.name=? ";
    private static final String SQL_FIND_ALL_PRODUCTS_WHERE_RESERVED_MORE_COUNT = SQL_FIND_ALL_PRODUCTS_WHERE + " and p.count > p.reserved ";

    private static final String SQL_FIND_ALL_PRODUCTS = "select  p.id, " +
            "p.name as name, " +
            "t.product_description_translation as description, " +
            "p.image_link, " +
            "p.price, " +
            "c.name as category, " +
            "pr.name as producer, " +
            "p.made_date as date, " +
            "p.count as count, " +
            "p.reserved as reserved " +
            "from product p " +
            "join category c on c.id = p.id_category " +
            "join producer pr on pr.id = p.id_producer " +
            "join translation t on p.id = t.product_id " +
            "join languages l on l.id = t.language_id";
    private static final String SQL_FIND_PRODUCT_BY_ID = "select p.id, " +
            "p.name as name, " +
            "p.description as description, " +
            "p.image_link, " +
            "p.price, " +
            "c.name as category, " +
            "pr.name as producer, " +
            "p.made_date as date, " +
            "p.count as count, " +
            "p.reserved as reserved " +
            "from product p " +
            "join category c on c.id = p.id_category " +
            "join producer pr on pr.id = p.id_producer " +
            "where p.id=?";
    private static final String SQL_DELETE_PRODUCT_BY_ID = "delete from product p where p.id=?";
    private static final String SQL_DELETE_PRODUCT_TRANSLATIONS = "delete from translation t where t.product_id=?";
    private static final String SQL_FIND_CATEGORY_ID_BY_NAME = "select c.id as id from category c where c.name=?";
    private static final String SQL_FIND_PRODUCER_ID_BY_NAME = "select pr.id as id from producer pr where pr.name=?";
    private static final String SQL_UPDATE_PRODUCT_BY_ID = "update product p set p.name=?, p.description=?, " +
            "p.image_link=?, p.price=?, p.made_date=?, p.id_producer=?, p.id_category=?, p.count=?, p.reserved=? where p.id=?";
    private static final String SQL_UPDATE_NAME_TRANSLATIONS_BY_PRODUCT_ID = "update translation t set t.product_name_translation=? where t.product_id=? and t.language_id=?";
    private static final String SQL_UPDATE_DESCRIPTION_TRANSLATIONS_BY_PRODUCT_ID = "update translation t set t.product_description_translation=? where t.product_id=? and t.language_id=?";
    private static final String SQL_INSERT_PRODUCT = "insert into product(name, description, image_link, price, made_date, id_producer, id_category, count, reserved) VALUES (?,?,?,?,?,?,?,?,?);";
    private static final String SQL_SELECT_PRODUCT_ID_BY_NAME = "select p.id from product p where p.name=?;";
    private static final String SQL_INSERT_NAME_TRANSLATION_BY_ID = "insert into translation(product_name_translation, product_description_translation, product_id, language_id) values (?,?,?,?);";
    private static final String SQL_FIND_ALL_PRODUCTS_GROUP_BY_ID = SQL_FIND_ALL_PRODUCTS + " group by p.id";

    /**
     * Find all products with localization and pagination.
     *
     * @param locale      locale name.
     * @param limit       limit for query.
     * @param noOfRecords last element which was taken.
     * @return list of products.
     */
    public List<Product> findProducts(String locale, int limit, int noOfRecords) {
        List<Product> productsList = new ArrayList<>();
        return getProducts(locale, productsList, SQL_FIND_ALL_PRODUCTS_WHERE + " limit " + limit + ", " + noOfRecords);
    }

    /**
     * Used for filter products which
     * reserved count more or equel to
     * count.
     *
     * @param locale      locale name.
     * @param limit       limit for query.
     * @param noOfRecords last taken element for query.
     * @return list of products.
     */
    public List<Product> findFilterProducts(String locale, int limit, int noOfRecords) {
        List<Product> productsList = new ArrayList<>();
        return getProducts(locale, productsList, SQL_FIND_ALL_PRODUCTS_WHERE_RESERVED_MORE_COUNT + " limit " + limit + ", " + noOfRecords);
    }

    /**
     * Find all products with localization, pagination and sorting.
     *
     * @param locale      locale name.
     * @param orderQuery  sql query.
     * @param limit       limit for query.
     * @param noOfRecords last taken element for query.
     * @return list of products.
     */
    public List<Product> findFilterProducts(String locale, String orderQuery, int limit, int noOfRecords) {
        List<Product> productsList = new ArrayList<>();
        String sql = SQL_FIND_ALL_PRODUCTS_WHERE_RESERVED_MORE_COUNT + orderQuery + " limit " + limit + ", " + noOfRecords;
        return getProducts(locale, productsList, sql);
    }

    /**
     * Find and add products to list by query with locale.
     *
     * @param locale       locale name.
     * @param productsList list of products.
     * @param sql          sql query.
     * @return updated list pf products.
     */
    private List<Product> getProducts(String locale, List<Product> productsList, String sql) {
        PreparedStatement statement;
        ResultSet resultSet;
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            ProductMapper productMapper = new ProductMapper();
            statement = connection.prepareStatement(sql);
            statement.setString(1, locale);
            statement.setString(2, locale);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                productsList.add(productMapper.mapRow(resultSet));
            }
            resultSet.close();
            resultSet = statement.executeQuery("select FOUND_ROWS()");
            while (resultSet.next()) {
                noOfRecords = resultSet.getInt(1);
            }
        } catch (SQLException throwables) {
            if (connection != null) {
                DBManager.getInstance().rollbackAndClose(connection);
            }
        } finally {
            if (connection != null)
                DBManager.getInstance().commitAndClose(connection);
        }
        return productsList;
    }

    @Override
    public List<Product> getAllElements() {
        return getAllElementsByQuery(new ProductMapper(), SQL_FIND_ALL_PRODUCTS_GROUP_BY_ID);
    }

    @Override
    public Product findElementById(long id) {
        return findElementByIdAndQuery(new ProductMapper(), SQL_FIND_PRODUCT_BY_ID, id);
    }

    @Override
    public void deleteElementById(long id) {
        changeById(id, SQL_DELETE_PRODUCT_BY_ID);
    }

    /**
     * Delete product translation after delete products.
     *
     * @param id product id.
     */
    public void deleteProductTranslationsById(long id) {
        changeById(id, SQL_DELETE_PRODUCT_TRANSLATIONS);
    }

    /**
     * Find category id by it name.
     *
     * @param name category name.
     * @return category id.
     */
    public int findCategoryIdByName(String name) {
        return findIdQuery(name, SQL_FIND_CATEGORY_ID_BY_NAME);
    }

    /**
     * Find producer id by it name
     *
     * @param name producer name.
     * @return producer id.
     */
    public int findProducerIdByName(String name) {
        return findIdQuery(name, SQL_FIND_PRODUCER_ID_BY_NAME);
    }

    @Override
    public void updateElement(Product element) {
        PreparedStatement statement;
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement(SQL_UPDATE_PRODUCT_BY_ID);
            assignProductToStatement(statement, element);
            statement.setLong(10, element.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            if (connection != null) {
                DBManager.getInstance().rollbackAndClose(connection);
            }
        } finally {
            if (connection != null) {
                DBManager.getInstance().commitAndClose(connection);
            }
        }
    }

    /**
     * Set product's fields to statement's query.
     *
     * @param statement Prepared statement object.
     * @param element   element for update.
     * @throws SQLException statement could throw sql exception.
     */
    private void assignProductToStatement(PreparedStatement statement, Product element) throws SQLException {
        int producerId = findProducerIdByName(element.getProducer());
        int categoryId = findCategoryIdByName(element.getCategory());
        statement.setString(1, element.getName());
        statement.setString(2, element.getDescription());
        statement.setString(3, element.getImageLink());
        statement.setBigDecimal(4, element.getPrice());
        statement.setTimestamp(5, new Timestamp(element.getMadeDate().getTime()));
        statement.setInt(6, producerId);
        statement.setInt(7, categoryId);
        statement.setInt(8, element.getCount());
        statement.setInt(9, element.getReserved());
    }

    /**
     * Update product name translations after update product.
     *
     * @param elementId    product id.
     * @param translations list of translations to update.
     */
    public void updateElementNameTranslations(Long elementId, List<String> translations) {
        for (int i = 0; i < translations.size(); i++) {
            int languageId = i + 1;
            updateSingleNameTranslation(elementId, translations.get(i), languageId);
        }
    }

    /**
     * Update product name translation for single language.
     *
     * @param elementId   product id.
     * @param translation translation to update.
     * @param languageId  language id.
     */
    public void updateSingleNameTranslation(Long elementId, String translation, int languageId) {
        translationUpdate(elementId, translation, languageId, SQL_UPDATE_NAME_TRANSLATIONS_BY_PRODUCT_ID);
    }

    /**
     * Update product description translations after update product.
     *
     * @param elementId    product id.
     * @param translations list of descriptions translations.
     */
    public void updateElementDescriptionTranslations(Long elementId, List<String> translations) {
        for (int i = 0; i < translations.size(); i++) {
            int languageId = i + 1;
            updateSingleDescriptionTranslation(elementId, translations.get(i), languageId);
        }
    }

    /**
     * Update product description translation for single language.
     *
     * @param elementId   product id.
     * @param translation description translation.
     * @param languageId  language id.
     */
    public void updateSingleDescriptionTranslation(Long elementId, String translation, int languageId) {
        translationUpdate(elementId, translation, languageId, SQL_UPDATE_DESCRIPTION_TRANSLATIONS_BY_PRODUCT_ID);
    }

    /**
     * Find product name translation by it id.
     *
     * @param elementId product id.
     * @return list of product name translations.
     */
    public List<String> findProductsNameTranslationsById(long elementId) {
        List<String> productNameTranslationList = new ArrayList<>();
        PreparedStatement statement;
        ResultSet resultSet;
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement(SQL_SELECT_NAME_TRANSLATIONS_WHERE_CATEGORY_ID);
            statement.setLong(1, elementId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                productNameTranslationList.add(resultSet.getString("name"));
            }
        } catch (SQLException e) {
            if (connection != null) {
                DBManager.getInstance().rollbackAndClose(connection);
            }
        } finally {
            if (connection != null) {
                DBManager.getInstance().commitAndClose(connection);
            }
        }
        return productNameTranslationList;
    }

    /**
     * Find product description translations by it id.
     *
     * @param elementId product id.
     * @return list of product description translations.
     */
    public List<String> findProductsDescriptionsTranslationsById(long elementId) {
        List<String> productDescriptionTranslationList = new ArrayList<>();
        PreparedStatement statement;
        ResultSet resultSet;
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement(SQL_SELECT_DESCRIPTIONS_TRANSLATIONS_WHERE_CATEGORY_ID);
            statement.setLong(1, elementId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                productDescriptionTranslationList.add(resultSet.getString("description"));
            }
        } catch (SQLException e) {
            if (connection != null) {
                DBManager.getInstance().rollbackAndClose(connection);
            }
        } finally {
            if (connection != null) {
                DBManager.getInstance().commitAndClose(connection);
            }
        }
        return productDescriptionTranslationList;
    }

    /**
     * Find product id by it's name.
     *
     * @param name product name.
     * @return product id.
     */
    public int findElementIdByName(String name) {
        return findIdQuery(name, SQL_SELECT_PRODUCT_ID_BY_NAME);
    }

    @Override
    public void insertElement(Product element) {
        PreparedStatement statement;
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement(SQL_INSERT_PRODUCT);
            assignProductToStatement(statement, element);
            statement.executeUpdate();
        } catch (SQLException e) {
            if (connection != null) {
                DBManager.getInstance().rollbackAndClose(connection);
            }
        } finally {
            if (connection != null) {
                DBManager.getInstance().commitAndClose(connection);
            }
        }
    }

    /**
     * Add product translations after product was added.
     *
     * @param elementId               product id.
     * @param nameTranslations        list of product name translations.
     * @param descriptionTranslations list of product descriptions translations.
     */
    public void insertTranslationsByProductId(int elementId, List<String> nameTranslations, List<String> descriptionTranslations) {
        for (int i = 0; i < nameTranslations.size(); i++) {
            insertSingleTranslationByProductId(elementId, nameTranslations.get(i), descriptionTranslations.get(i), i + 1);
        }
    }

    /**
     * Add product translation by single language.
     *
     * @param elementId              product id.
     * @param nameTranslation        product name translation.
     * @param descriptionTranslation product description translation.
     * @param languageId             language id.
     */
    public void insertSingleTranslationByProductId(int elementId, String nameTranslation, String descriptionTranslation, int languageId) {
        PreparedStatement statement;
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement(SQL_INSERT_NAME_TRANSLATION_BY_ID);
            statement.setNString(1, nameTranslation);
            statement.setNString(2, descriptionTranslation);
            statement.setLong(3, elementId);
            statement.setInt(4, languageId);
            statement.executeUpdate();
        } catch (SQLException e) {
            if (connection != null) {
                DBManager.getInstance().rollbackAndClose(connection);
            }
        } finally {
            if (connection != null) {
                DBManager.getInstance().commitAndClose(connection);
            }
        }
    }

    /**
     * Get number of last showed element,
     * using for pagination.
     *
     * @return last showed element.
     */
    public int getNoOfRecords() {
        return noOfRecords;
    }

    public void removeReserveForSingleProduct(long id) {
        changeById(id, SQL_DELETE_RESERVE_FOR_SINGLE_PRODUCT);
    }

    public void removeAllReserved() {
        PreparedStatement statement;
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement(SQL_REMOVE_ALL_RESERVED);
            statement.executeUpdate();
        } catch (SQLException e) {
            if (connection != null) {
                DBManager.getInstance().rollbackAndClose(connection);
            }
        } finally {
            if (connection != null) {
                DBManager.getInstance().commitAndClose(connection);
            }
        }
    }

    /**
     * Mapper for product entity.
     *
     * @author Denys Filatov
     */
    private static class ProductMapper implements EntityMapper<Product> {
        @Override
        public Product mapRow(ResultSet rs) {
            try {
                Product product = new Product();
                product.setId(rs.getLong("id"));
                product.setName(rs.getString("name"));
                product.setDescription(rs.getString("description"));
                product.setImageLink(rs.getString("image_link"));
                product.setId(rs.getLong("id"));
                product.setPrice(rs.getBigDecimal("price"));
                product.setCategory(rs.getString("category"));
                product.setProducer(rs.getString("producer"));
                product.setMadeDate(rs.getTimestamp("date"));
                product.setCount(rs.getInt("count"));
                product.setReserved(rs.getInt("reserved"));
                return product;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
