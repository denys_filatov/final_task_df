package com.dfilatov.istore.db;

import com.dfilatov.istore.db.entity.Account;

/**
 * Account_Role entity
 *
 * @author Denys Filatov
 */
public enum AccountRole {
    ADMIN, USER;

    public static AccountRole getRole(Account account) {
        for(AccountRole role : AccountRole.values()){
            if(role.getName().equals(account.getRole())){
                return role;
            }
        }
        return null;
    }

    public static AccountRole getRoleByName(String value){
        for(AccountRole role : AccountRole.values()){
            if(role.getName().equalsIgnoreCase(value)){
                return role;
            }
        }
        return null;
    }

    public String getName() {
        return name().toLowerCase();
    }
}
