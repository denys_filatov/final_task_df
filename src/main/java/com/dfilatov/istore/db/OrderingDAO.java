package com.dfilatov.istore.db;

import com.dfilatov.istore.db.entity.Ordering;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * DAO for Ordering entity
 *
 * @author Denys Filatov
 */
public class OrderingDAO implements AbstractDAO<Ordering> {

    private static final String SQL_FIND_ALL_ORDERS = "select o.id, a.name as account, o.created, os.name as status " +
            "from ordering o " +
            "join account a on a.id = o.id_account " +
            "join order_status os on os.id = o.id_status order by o.id_status";
    private static final String SQL_FIND_ORDERING_BY_ID = "select o.id, a.name as account, o.created, os.name as status " +
            "from ordering o " +
            "join account a on a.id = o.id_account " +
            "join order_status os on os.id = o.id_status " +
            "where o.id=?";
    private static final String SQL_DELETE_ORDERING_BY_ID = "delete from ordering o where o.id=?";
    private static final String SQL_FIND_STATUS_ID_BY_NAME = "select os.id from order_status os where os.name=?";
    private static final String SQL_INSERT_ORDER = "insert into ordering(id_account, created, id_status) values (?,?,?)";
    private static final String SQL_FIND_ACCOUNT_ID_BY_NAME = "select a.id from account a where a.name=?";
    private static final String SQL_INSERT_ORDER_ITEM = "insert into order_item(id_order, id_product, count) values(?,?,?)";
    private static final String SQL_DELETE_ORDER_ITEM_BY_ORDER_ID = "delete from order_item where id_order=?";
    private static final String SQL_UPDATE_ORDER_STATUS = "update ordering set id_status=? where id=?";
    private static final String SQL_FIND_PODUCT_AND_COUNT_BY_ORDER_ID = "select oi.id_product as productId, oi.count as count from order_item oi where id_order=?";
    private static final String SQL_FIND_ORDERS_ID_BY_USER_ID = "select o.id as orderId from ordering o where id_account=?";

    private long lastInsertedId;

    @Override
    public Ordering findElementById(long id) {
        return findElementByIdAndQuery(new OrderingMapper(), SQL_FIND_ORDERING_BY_ID, id);
    }

    @Override
    public List<Ordering> getAllElements() {
        return getAllElementsByQuery(new OrderingMapper(), SQL_FIND_ALL_ORDERS);
    }

    @Override
    public void deleteElementById(long id) {
        changeById(id, SQL_DELETE_ORDERING_BY_ID);
    }

    @Override
    public void updateElement(Ordering element) {
        PreparedStatement statement;
        Connection connection = null;
        try {
            int statusId = findIdQuery(element.getOrderStatus(), SQL_FIND_STATUS_ID_BY_NAME);
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement(SQL_UPDATE_ORDER_STATUS);
            statement.setLong(1, statusId);
            statement.setLong(2, element.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            if (connection != null) {
                DBManager.getInstance().rollbackAndClose(connection);
            }
        } finally {
            if (connection != null) {
                DBManager.getInstance().commitAndClose(connection);
            }
        }
    }

    @Override
    public void insertElement(Ordering element) {
        PreparedStatement statement;
        Connection connection = null;
        try {
            int accountId = getAccountIdByName(element.getAccount());
            int statusId = getStatusIdByName(element.getOrderStatus());
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement(SQL_INSERT_ORDER);
            statement.setInt(1, accountId);
            statement.setTimestamp(2, element.getCreated());
            statement.setInt(3, statusId);
            statement.executeUpdate();
            ResultSet resultSet = statement.executeQuery("select LAST_INSERT_ID()");
            while (resultSet.next()) {
                lastInsertedId = resultSet.getLong(1);
            }
        } catch (SQLException e) {
            if (connection != null) {
                DBManager.getInstance().rollbackAndClose(connection);
            }
        } finally {
            if (connection != null) {
                DBManager.getInstance().commitAndClose(connection);
            }
        }
    }

    /**
     * Find account id by its name.
     *
     * @param accountName account name
     * @return account.
     */
    private int getAccountIdByName(String accountName) {
        return findIdQuery(accountName, SQL_FIND_ACCOUNT_ID_BY_NAME);
    }

    /**
     * Find status id by it name.
     *
     * @param statusName status name.
     * @return status id.
     */
    private int getStatusIdByName(String statusName) {
        return findIdQuery(statusName, SQL_FIND_STATUS_ID_BY_NAME);
    }

    /**
     * Find id of last inserted element.
     *
     * @return id of last inserted element.
     */
    public long getLastInsertedId() {
        return lastInsertedId;
    }

    /**
     * Insert new order's items to order_item table
     *
     * @param orderId           order id.
     * @param productsAndCounts map of products and it's counts.
     */
    public void insertOrderItems(long orderId, Map<Long, Integer> productsAndCounts) {
        productsAndCounts.forEach((k, v) -> insertSingleOrderItem(orderId, k, v));
    }

    /**
     * Insert single order's item to order_item table.
     *
     * @param orderId         order id.
     * @param productId       product id.
     * @param countOfProducts count of products.
     */
    private void insertSingleOrderItem(long orderId, Long productId, Integer countOfProducts) {
        PreparedStatement statement;
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement(SQL_INSERT_ORDER_ITEM);
            statement.setLong(1, orderId);
            statement.setLong(2, productId);
            statement.setInt(3, countOfProducts);
            statement.executeUpdate();
        } catch (SQLException e) {
            if (connection != null) {
                DBManager.getInstance().rollbackAndClose(connection);
            }
        } finally {
            if (connection != null) {
                DBManager.getInstance().commitAndClose(connection);
            }
        }
    }

    /**
     * Change order's status to COMPLETE.
     *
     * @param ordering ordering to change.
     */
    public void completeOrder(Ordering ordering) {
        ordering.setOrderStatus(OrderStatus.COMPLETED.name());
        updateElement(ordering);
    }

    /**
     * Change order status to cancelled
     *
     * @param ordering ordering to change.
     */
    public void cancelOrder(Ordering ordering) {
        ordering.setOrderStatus(OrderStatus.CANCELED.name());
        updateElement(ordering);
    }

    /**
     * Find order's products and its count.
     *
     * @param id order id.
     * @return map of products and it's counts.
     */
    public Map<Long, Integer> findProductAndCountFromOrderByOrderId(Long id) {
        Map<Long, Integer> productAndCount = new LinkedHashMap<>();
        PreparedStatement statement;
        ResultSet resultSet;
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement(SQL_FIND_PODUCT_AND_COUNT_BY_ORDER_ID);
            statement.setLong(1, id);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Long productId = resultSet.getLong("productId");
                int count = resultSet.getInt("count");
                productAndCount.put(productId, count);
            }
        } catch (SQLException e) {
            if (connection != null) {
                DBManager.getInstance().rollbackAndClose(connection);
            }
        } finally {
            if (connection != null) {
                DBManager.getInstance().commitAndClose(connection);
            }
        }
        return productAndCount;
    }

    /**
     * Find order's ids for single user.
     *
     * @param id user id.
     * @return list of orders ids.
     */
    public List<Long> findOrdersIdByUserId(Integer id) {
        List<Long> ordersId = new ArrayList<>();
        PreparedStatement statement;
        ResultSet resultSet;
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement(SQL_FIND_ORDERS_ID_BY_USER_ID);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Long orderId = resultSet.getLong("orderId");
                ordersId.add(orderId);
            }
        } catch (SQLException e) {
            if (connection != null) {
                DBManager.getInstance().rollbackAndClose(connection);
            }
        } finally {
            if (connection != null) {
                DBManager.getInstance().commitAndClose(connection);
            }
        }
        return ordersId;
    }

    /**
     * Delete order items before delete order.
     *
     * @param orderId oder id.
     */
    public void deleteOrderItemsByOrderId(long orderId) {
        changeById(orderId, SQL_DELETE_ORDER_ITEM_BY_ORDER_ID);
    }


    /**
     * Mapper for ordering entity.
     *
     * @author Denys Filatov
     */
    private static class OrderingMapper implements EntityMapper<Ordering> {
        @Override
        public Ordering mapRow(ResultSet rs) {
            try {
                Ordering ordering = new Ordering();
                ordering.setId(rs.getLong("id"));
                ordering.setAccount(rs.getString("account"));
                ordering.setCreated(rs.getTimestamp("created"));
                ordering.setOrderStatus(rs.getString("status"));
                return ordering;
            } catch (SQLException throwables) {
                throw new IllegalStateException(throwables);
            }
        }
    }
}
