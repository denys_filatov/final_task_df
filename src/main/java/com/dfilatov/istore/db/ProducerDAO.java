package com.dfilatov.istore.db;

import com.dfilatov.istore.db.entity.Producer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * DAO for producer entity
 *
 * @author Denys Filatov
 */
public class ProducerDAO implements AbstractDAO<Producer>, CountSynchronizable<Producer> {

    private static final String SQL_FIND_ALL_PRODUCERS = "select * from producer p";
    private static final String SQL_FIND_PRODUCER_BY_ID = SQL_FIND_ALL_PRODUCERS + " where p.id=?;";
    private static final String SQL_DELETE_PRODUCER_BY_ID = "delete from producer pr where pr.id=?";
    private static final String SQL_FIND_COUNT_OF_PRODUCER_PRODUCTS = "select sum(p.count) as count from product p where p.id_producer=?;";
    private static final String SQL_UPDATE_ELEMENT_BY_ID = "update producer pr set pr.name=? where pr.id=?";
    private static final String SQL_INSERT_PRODUCER = "insert into producer(name) VALUE (?);";
    private static final String SQL_UPDATE_COUNT_OF_PRODUCER_ELEMENTS = "update producer set producer.product_count=" +
            "(SELECT sum(count) as productCount from product where product.id_producer=?) where producer.id=?;";

    @Override
    public List<Producer> getAllElements() {
        return getAllElementsByQuery(new ProducerMapper(), SQL_FIND_ALL_PRODUCERS);
    }

    /**
     * Find count of single producer items by it id.
     *
     * @param id producer id.
     * @return count of products.
     */
    public int findCountOfProducerProducts(long id) {
        return findCount(id, SQL_FIND_COUNT_OF_PRODUCER_PRODUCTS);
    }

    @Override
    public Producer findElementById(long id) {
        return findElementByIdAndQuery(new ProducerMapper(), SQL_FIND_PRODUCER_BY_ID, id);
    }

    @Override
    public void deleteElementById(long id) {
        changeById(id, SQL_DELETE_PRODUCER_BY_ID);
    }

    @Override
    public void updateElement(Producer element) {
        PreparedStatement statement;
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement(SQL_UPDATE_ELEMENT_BY_ID);
            statement.setString(1, element.getName());
            statement.setLong(2, element.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            if (connection != null) {
                DBManager.getInstance().rollbackAndClose(connection);
            }
        } finally {
            if (connection != null) {
                DBManager.getInstance().commitAndClose(connection);
            }
        }
    }

    @Override
    public void insertElement(Producer element) {
        PreparedStatement statement;
        Connection connection = null;
        try {
            connection = DBManager.getInstance().getConnection();
            statement = connection.prepareStatement(SQL_INSERT_PRODUCER);
            statement.setString(1, element.getName());
            statement.executeUpdate();
        } catch (SQLException e) {
            if (connection != null) {
                DBManager.getInstance().rollbackAndClose(connection);
            }
        } finally {
            if (connection != null) {
                DBManager.getInstance().commitAndClose(connection);
            }
        }
    }

    /**
     * Update elements count for all producers.
     *
     * @param elements list of producers.
     */
    @Override
    public void updateElementsCount(List<Producer> elements) {
        for (Producer producer : elements) {
            updateSingleElementCount(producer);
        }
    }

    /**
     * Update elements count for single producer.
     *
     * @param element producer element.
     */
    @Override
    public void updateSingleElementCount(Producer element) {
        updateCount(element.getId(), SQL_UPDATE_COUNT_OF_PRODUCER_ELEMENTS);
    }

    /**
     * Mapper for producer entity.
     *
     * @author Denys Filatov
     */
    private static class ProducerMapper implements EntityMapper<Producer> {
        @Override
        public Producer mapRow(ResultSet rs) {
            try {
                Producer producer = new Producer();
                producer.setId(rs.getInt("id"));
                producer.setName(rs.getString("name"));
                producer.setCount(rs.getInt("product_count"));
                return producer;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
    }


}
