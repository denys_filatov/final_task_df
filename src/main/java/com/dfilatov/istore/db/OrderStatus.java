package com.dfilatov.istore.db;

/**
 * Order_status entity
 *
 * @author Denys Filatov
 */
public enum OrderStatus {
    PENDING, COMPLETED, CANCELED
}
