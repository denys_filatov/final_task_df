package com.dfilatov.istore.model;

import com.dfilatov.istore.db.entity.Product;

import java.io.Serializable;
import java.util.StringJoiner;

/**
 * Implements item of user's cart.
 * Product and its count of items.
 *
 * @author Denys Filatov
 */
public class CartItem implements Serializable {
    private static final long serialVersionUID = 7142117777136284203L;

    private Product product;
    private int count;

    public CartItem() {
    }

    public CartItem(Product product, int count) {
        this.product = product;
        this.count = count;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", CartItem.class.getSimpleName() + "[", "]")
                .add("product=" + product)
                .add("count=" + count)
                .toString();
    }
}
