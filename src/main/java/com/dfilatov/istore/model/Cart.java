package com.dfilatov.istore.model;

import com.dfilatov.istore.PathConstants;
import com.dfilatov.istore.db.entity.Product;
import com.dfilatov.istore.exception.IStoreException;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.StringJoiner;

/**
 * Implements user's shopping cart
 *
 * @author Denys Filatov
 */
public class Cart implements Serializable {
    private static final long serialVersionUID = 7542513416568070309L;
    private final Map<Long, CartItem> products = new LinkedHashMap<>();
    private int totalCount = 0;
    private BigDecimal totalCost = BigDecimal.ZERO;

    /**
     * Add product to the cart
     *
     * @param product product element.
     * @param count   count of products.
     * @throws IllegalArgumentException could be throws during validation.
     */
    public void addProduct(Product product, int count) throws IStoreException{
        validateCartSize(product.getId());
        CartItem cartItem = products.get(product.getId());
        if (cartItem == null) {
            validateProductCount(count);
            cartItem = new CartItem(product, count);
            products.put(product.getId(), cartItem);
        } else {
            validateProductCount(count + cartItem.getCount());
            cartItem.setCount(cartItem.getCount() + count);
        }
        refreshStatistics();
    }

    /**
     * Validate count of products in the cart
     * before adding new product.
     *
     * @param id product id.
     */
    private void validateCartSize(Long id) throws IStoreException{
        if (products.size() > PathConstants.MAX_PRODUCTS_PER_CART ||
                (products.size() == PathConstants.MAX_PRODUCTS_PER_CART && !products.containsKey(id))) {
            throw new IStoreException("Limit for product count is reached", new IllegalArgumentException());
        }
    }

    /**
     * Validate count of single product's items
     * in the cart before adding new one.
     *
     * @param count product count.
     */
    private void validateProductCount(int count) throws IStoreException {
        if (count > PathConstants.MAX_PRODUCT_COUNT_PER_CART) {
            throw new IStoreException("Limit for product count is reached", new IllegalArgumentException());
        }
    }

    /**
     * Remove product from the cart
     *
     * @param productId product id.
     * @param count     products count.
     */
    public void removeProduct(long productId, int count) {
        CartItem cartItem = products.get(productId);

        if (cartItem != null) {
            if (cartItem.getCount() > count) {
                cartItem.setCount(cartItem.getCount() - count);
            } else {
                products.remove(productId);
            }
            refreshStatistics();
        }
    }

    public Collection<CartItem> getItems() {
        return products.values();
    }

    public int getTotalCount() {
        return totalCount;
    }

    public BigDecimal getTotalCost() {
        return totalCost;
    }

    /**
     * Update total cost and total count
     * in the cart.
     */
    private void refreshStatistics() {
        totalCount = 0;
        totalCost = BigDecimal.ZERO;
        for (CartItem item : getItems()) {
            totalCount += item.getCount();
            totalCost = totalCost.add(item.getProduct().getPrice()).multiply(BigDecimal.valueOf(item.getCount()));
        }
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Cart.class.getSimpleName() + "[", "]")
                .add("products=" + products)
                .add("totalCount=" + totalCount)
                .add("totalCost=" + totalCost)
                .toString();
    }
}
