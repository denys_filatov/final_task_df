package com.dfilatov.istore;

/**
 * Hold path constants for jsp pages, and controller commands.
 *
 * @author Denys Filatov
 */
public class PathConstants {
    public static final String PAGE_PRODUCTS_LIST = "products.jsp";
    public static final String PAGE_REDACTOR = "redactor.jsp";
    public static final String PAGE_UPDATE = "update.jsp";
    public static final String PAGE_LOGIN = "/WEB-INF/jsp/page/login.jsp";
    public static final String PAGE_CART = "/WEB-INF/jsp/page/shopping-cart.jsp";
    public static final String PAGE_ACCOUNT = "/WEB-INF/jsp/page/personalAccount.jsp";
    public static final String PAGE_CREATE_ACCOUNT = "/WEB-INF/jsp/page/registration.jsp";
    public static final String PAGE_PAGE_TEMPLATE = "/WEB-INF/jsp/page-template.jsp";
    public static final String CURRENT_PAGE = "currentPage";
    public static final String PAGE_ERROR_PAGE = "/WEB-INF/jsp/page/error.jsp";
    public static final String PAGE_ADMIN = "/WEB-INF/jsp/admin.jsp";

    public static final String ADMIN_COMMAND = "/controller?command=adminCommand";
    public static final String COMMAND_LIST_PRODUCTS = "/controller?command=listProducts";
    public static final String COMMAND_ACCOUNT = "/controller?command=toAccount";
    public static final String COMMAND_ORDER_REDACTOR = "/controller?command=redactor&type=ordering";

    public static final String DEFAULT_LOCALE = "defaultLocale";
    public static final String USER_ROLE = "userRole";
    public static final String ERROR_MESSAGE = "errorMessage";
    public static final String CURRENT_SHOPPING_CART = "CURRENT_CART";
    public static final String ERROR_STRING = "error";

    public static final int MAX_PRODUCT_COUNT_PER_CART = 5;
    public static final int MAX_PRODUCTS_PER_CART = 20;
}
