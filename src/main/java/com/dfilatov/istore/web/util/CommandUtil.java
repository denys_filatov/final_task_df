package com.dfilatov.istore.web.util;

import com.dfilatov.istore.PathConstants;
import com.dfilatov.istore.db.CategoryDAO;
import com.dfilatov.istore.db.ProductsDAO;
import com.dfilatov.istore.db.entity.Category;
import com.dfilatov.istore.db.entity.Product;
import com.dfilatov.istore.model.Cart;
import org.apache.log4j.Logger;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.math.BigDecimal;
import java.net.ProtocolException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Implements often used command's methods
 *
 * @author Denys Filatov
 */
public class CommandUtil {
    private static final Logger LOGGER = Logger.getLogger(CommandUtil.class);

    private CommandUtil() {
    }

    public static void removeAllReserved(HttpServletRequest request) {
        Cart cart = (Cart) request.getSession().getAttribute(PathConstants.CURRENT_SHOPPING_CART);
        ProductsDAO productsDAO = new ProductsDAO();
        if (cart != null)
            cart.getItems().forEach(p -> productsDAO.removeReserveForSingleProduct(p.getProduct().getId()));
    }

    public static void removeAllReserved() {
        ProductsDAO productsDAO = new ProductsDAO();
        productsDAO.removeAllReserved();
    }

    public static void getProductsAndCategories(HttpServletRequest request, String query) {
        String locale = request.getSession().getAttribute(PathConstants.DEFAULT_LOCALE).toString();
        LOGGER.debug("Locale is " + locale);


        List<Category> categoryList = new CategoryDAO().findCategory(locale);

        LOGGER.debug("Found in DB: categoryList -> " + categoryList);

        int page = 1;
        int recordPerPage = 12;
        if (request.getParameter("page") != null) {
            page = Integer.parseInt(request.getParameter("page"));
        }
        List<Product> productList;
        ProductsDAO productsDAO = new ProductsDAO();
        productList = productsDAO.findFilterProducts(locale, query, (page - 1) * recordPerPage, recordPerPage);
        setProductAndCategoriesToRequest(request, categoryList, page, recordPerPage, productList, productsDAO);
    }

    public static void getProductsAndCategories(HttpServletRequest request) {
        String locale = request.getSession().getAttribute(PathConstants.DEFAULT_LOCALE).toString();
        LOGGER.debug("Locale is " + locale);

        List<Category> categoryList = new CategoryDAO().findCategory(locale);
        LOGGER.debug("Found in DB: categoryList -> " + categoryList);

        int page = 1;
        int recordPerPage = 12;
        if (request.getParameter("page") != null) {
            page = Integer.parseInt(request.getParameter("page"));
        }
        List<Product> productList;
        ProductsDAO productsDAO = new ProductsDAO();
        productList = productsDAO.findFilterProducts(locale, (page - 1) * recordPerPage, recordPerPage);
        if (request.getParameter("command") != null && request.getParameter("command").equals("listProducts")) {
            productList.removeIf(product -> product.getCount() <= product.getReserved());
        }
        setProductAndCategoriesToRequest(request, categoryList, page, recordPerPage, productList, productsDAO);
    }

    private static void setProductAndCategoriesToRequest(HttpServletRequest request, List<Category> categoryList, int page, int recordPerPage, List<Product> productList, ProductsDAO productsDAO) {
        int noOfRecords = productsDAO.getNoOfRecords();
        int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordPerPage);
        request.setAttribute("noOfPages", noOfPages);
        request.setAttribute("page", page);

        request.setAttribute("categories", categoryList);
        LOGGER.debug("Set the request attribute: categories " + categoryList);

        request.setAttribute("products", productList);
        LOGGER.trace("Set the request attribute: products: " + productList);
    }

    public static void updateProductById(HttpServletRequest request, Product product) {
        String productName = request.getParameter("productName");
        product.setName(productName);
        String productDescription = request.getParameter("productDescription");
        product.setDescription(productDescription);
        String productImageLink = request.getParameter("productImageLink");
        product.setImageLink(productImageLink);
        BigDecimal productPrice = BigDecimal.valueOf(Double.parseDouble(request.getParameter("productPrice")));
        product.setPrice(productPrice);
        String productCategory = request.getParameter("productCategory");
        product.setCategory(productCategory);
        String productProducer = request.getParameter("productProducer");
        product.setProducer(productProducer);
        String productMadeDate = request.getParameter("productMadeDate");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date madeDate = new Date();
        try {
            madeDate = dateFormat.parse(productMadeDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        product.setMadeDate(madeDate);
        int productCount = Integer.parseInt(request.getParameter("productCount"));
        product.setCount(productCount);
    }

    public static String getRequestQuery(HttpServletRequest request) {
        StringBuilder resultQueryBuilder = new StringBuilder();
        StringBuilder jspQueryBuilder = new StringBuilder();
        String priceFrom = getRequestParameter(request, "priceFrom");
        String priceTo = getRequestParameter(request, "priceTo");
        String searchName = getRequestParameter(request, "searchName");
        String categoryName = getRequestParameter(request, "categoryName");
        String orderQuery = getRequestParameter(request, "orderQuery");

        if ((priceFrom != null && priceTo != null) && (!priceFrom.isEmpty() && !priceTo.isEmpty())) {
            resultQueryBuilder.append(" and price>=").append(priceFrom).append(" and price<=").append(priceTo);
            request.setAttribute("priceFrom", priceFrom);
            request.setAttribute("priceTo", priceTo);
            jspQueryBuilder.append("&priceFrom=")
                    .append(priceFrom)
                    .append("&priceTo=")
                    .append(priceTo);
        }
        if (searchName != null && !searchName.isEmpty()) {
            resultQueryBuilder.append(" and p.name like '").append(searchName).append("%'");
            request.setAttribute("searchName", searchName);
            jspQueryBuilder.append("&searchName=")
                    .append(searchName);

        }
        if (categoryName != null && !categoryName.isEmpty()) {
            resultQueryBuilder.append(" and cl.category_name_translation='").append(categoryName).append("'");
            request.setAttribute("categoryName", categoryName);
            jspQueryBuilder.append("&categoryName=")
                    .append(categoryName);
        }
        if (orderQuery != null && !orderQuery.isEmpty()) {
            resultQueryBuilder.append(orderQuery);
            request.setAttribute("orderQuery", orderQuery);
            jspQueryBuilder.append("&orderQuery=")
                    .append(orderQuery);
        }
        request.setAttribute("query", jspQueryBuilder.toString());
        return resultQueryBuilder.toString();
    }

    private static String getRequestParameter(HttpServletRequest request, String parameterName) {
        String result = null;
        try {
            result = request.getParameter(parameterName);
        } catch (NullPointerException e) {
            LOGGER.info(parameterName + " parameter is absent in request");
        }
        return result;
    }

    public static List<String> getProductNameTranslation(HttpServletRequest request) {
        List<String> productNameTranslations = new ArrayList<>();
        String productUaName = request.getParameter("productUaName");
        productNameTranslations.add(productUaName);
        String productEnName = request.getParameter("productEnName");
        productNameTranslations.add(productEnName);
        return productNameTranslations;
    }

    public static List<String> getProductDescriptionTranslation(HttpServletRequest request) {
        List<String> productDescriptionTranslations = new ArrayList<>();
        String productUaDescription = request.getParameter("productUaDescription");
        productDescriptionTranslations.add(productUaDescription);
        String productEnDescription = request.getParameter("productEnDescription");
        productDescriptionTranslations.add(productEnDescription);
        return productDescriptionTranslations;
    }

    public static boolean verify(String gRecaptchaResponse) throws IOException {
        String url = "https://www.google.com/recaptcha/api/siteverify";
        String secret = "6LcH1dcZAAAAAInCDtz27wZHZgDjlXdvvFMf4uCw";
        String userAgent = "Mozilla/5.0";
        if (gRecaptchaResponse == null || "".equals(gRecaptchaResponse)) {
            return false;
        }

        URL obj = new URL(url);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

        // add reuqest header
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", userAgent);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        String postParams = "secret=" + secret + "&response="
                + gRecaptchaResponse;

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(postParams);
        wr.flush();
        wr.close();

        BufferedReader in = new BufferedReader(new InputStreamReader(
                con.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //parse JSON response and return 'success' value
        JsonReader jsonReader = Json.createReader(new StringReader(response.toString()));
        JsonObject jsonObject = jsonReader.readObject();
        jsonReader.close();

        return jsonObject.getBoolean("success");
    }
}
