package com.dfilatov.istore.web.util;

import com.dfilatov.istore.model.Cart;
import com.dfilatov.istore.PathConstants;

import javax.servlet.http.HttpServletRequest;

/**
 * Util class for control sholling cart.
 *
 * @author Denys Filatov
 */
public class SessionUtil {
    public static Cart getCurrentCart(HttpServletRequest request) {
        return (Cart) request.getSession().getAttribute(PathConstants.CURRENT_SHOPPING_CART);
    }

    public static boolean isCurrentCartIsCreated(HttpServletRequest request) {
        return request.getSession().getAttribute(PathConstants.CURRENT_SHOPPING_CART) != null;
    }

    public static void setCurrentCart(HttpServletRequest request, Cart cart) {
        request.getSession().setAttribute(PathConstants.CURRENT_SHOPPING_CART, cart);
    }

    public static void clearCart(HttpServletRequest request) {
        request.getSession().removeAttribute(PathConstants.CURRENT_SHOPPING_CART);
    }
}
