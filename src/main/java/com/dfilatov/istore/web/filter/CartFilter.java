package com.dfilatov.istore.web.filter;

import com.dfilatov.istore.model.Cart;
import com.dfilatov.istore.PathConstants;
import com.dfilatov.istore.db.AccountRole;
import com.dfilatov.istore.web.util.SessionUtil;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Shopping cart filter.
 *
 * @author Denys Filatov
 */
public class CartFilter implements Filter {
    private static final Logger LOGGER = Logger.getLogger(CartFilter.class);

    /**
     * Create cart if role is not 'admin'
     *
     * @param servletRequest Servlet Request object.
     * @param servletResponse ServletResponse object.
     * @param filterChain FilterChain objecy.
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        LOGGER.debug("Cart filter is start");
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String role;
        if (request.getSession().getAttribute(PathConstants.USER_ROLE) != null) {
            role = request.getSession().getAttribute(PathConstants.USER_ROLE).toString();
        } else {
            role = null;
        }
        if (role == null || role.isEmpty() || !role.equalsIgnoreCase(AccountRole.ADMIN.toString())) {
            if (!SessionUtil.isCurrentCartIsCreated(request)) {
                Cart cart = new Cart();
                SessionUtil.setCurrentCart(request, cart);
                LOGGER.debug("CURRENT cart created.");
            }
        }
        LOGGER.debug("Cart filter is finished");
        filterChain.doFilter(request, response);
    }
}
