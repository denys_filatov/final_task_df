package com.dfilatov.istore.web.filter;

import com.dfilatov.istore.PathConstants;
import com.dfilatov.istore.db.AccountRole;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

/**
 * Control command access by role.
 *
 * @author Denys Filatov
 */
public class CheckAuthentificationFilter implements Filter {
    private static final Logger LOGGER = Logger.getLogger(CheckAuthentificationFilter.class);

    private static final Map<AccountRole, List<String>> accessMap = new HashMap<>();
    private static List<String> mutual = new ArrayList<>();

    /**
     * Get command list from web.xml
     *
     * @param filterConfig FilterConfig object
     * @throws ServletException
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        LOGGER.debug("Filter initialization starts");

        accessMap.put(AccountRole.ADMIN, asList(filterConfig.getInitParameter("admin")));
        accessMap.put(AccountRole.USER, asList(filterConfig.getInitParameter("user")));
        mutual.addAll(asList(filterConfig.getInitParameter("mutual")));
        LOGGER.debug(mutual);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        LOGGER.debug("Role filter is working");
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        if (accessAllowed(request)) {
            LOGGER.debug("Filter finished");
            filterChain.doFilter(request, response);
        } else {
            String errorMessage = "You do not have permission to access this page";

            request.setAttribute("errorMessage", errorMessage);

            request.getRequestDispatcher(PathConstants.PAGE_ERROR_PAGE)
                    .forward(request, response);
        }
    }

    private boolean accessAllowed(HttpServletRequest request) {
        String commandName = request.getParameter("command");

        if (commandName == null || commandName.isEmpty()) {
            return false;
        }

        if (mutual.contains(commandName)) {
            return true;
        }

        HttpSession session = request.getSession(false);
        if (session == null)
            return false;

        AccountRole userRole = (AccountRole) session.getAttribute(PathConstants.USER_ROLE);
        if (userRole == null)
            return false;

        return accessMap.get(userRole).contains(commandName);
    }


    /**
     * Extracts parameter values from string.
     *
     * @param str parameter values string.
     * @return list of parameter values.
     */
    private List<String> asList(String str) {
        List<String> list = new ArrayList<>();
        StringTokenizer st = new StringTokenizer(str);
        while (st.hasMoreTokens()) list.add(st.nextToken());
        return list;
    }
}
