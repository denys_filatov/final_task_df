package com.dfilatov.istore.web.listener;

import com.dfilatov.istore.web.util.CommandUtil;
import org.apache.log4j.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Context Listener
 *
 * @author Denys Filatov
 */
public class ContextListener implements ServletContextListener {
    private static final Logger LOGGER = Logger.getLogger(ContextListener.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        log("Servlet context initialization starts!");

        ServletContext servletContext = sce.getServletContext();
        initI18N(servletContext);

        log("Servlet context initialization finished!");

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        log("Servlet context destruction starts");
        CommandUtil.removeAllReserved();
        log("Reserve was cleared");
        log("Servlet context destruction finished");
    }

    /**
     * Initializes i18n subsystem.
     */
    private void initI18N(ServletContext servletContext) {
        log("I18N subsystem initialization started");

        String localesValue = servletContext.getInitParameter("locales");
        if (localesValue == null || localesValue.isEmpty()) {
            LOGGER.warn("'locales' init parameter is empty, the default encoding will be used");
        } else {
            List<String> locales = new ArrayList<>();
            StringTokenizer st = new StringTokenizer(localesValue);
            while (st.hasMoreTokens()) {
                String localeName = st.nextToken();
                locales.add(localeName);
            }

            log("Application attribute set: locales --> " + locales);
            servletContext.setAttribute("locales", locales);
        }
        String defaultLocale = servletContext.getInitParameter("javax.servlet.jsp.jstl.fmt.locale");
        if (defaultLocale != null) {
            log("Application set attribute: defaultLocale --> " + defaultLocale);
            servletContext.setAttribute("defaultLocale", defaultLocale);
        }

        log("I18N subsystem initialization finished");
    }

    private void log(String message) {
        Logger.getLogger(ContextListener.class).debug("[ContextListener] " + message);
    }
}
