package com.dfilatov.istore.web.command.user;

import com.dfilatov.istore.db.OrderingDAO;
import com.dfilatov.istore.db.ProductsDAO;
import com.dfilatov.istore.db.entity.Account;
import com.dfilatov.istore.db.entity.Ordering;
import com.dfilatov.istore.model.CartItem;
import com.dfilatov.istore.web.command.Command;
import com.dfilatov.istore.PathConstants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * Redicert to personal account and show list of orders.
 *
 * @author Denys Filatov
 */
public class ToAccountCommand extends Command {
    private static final long serialVersionUID = -6392466000513778861L;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Account account = (Account) request.getSession().getAttribute("account");
        OrderingDAO orderingDAO = new OrderingDAO();
        ProductsDAO productsDAO = new ProductsDAO();
        List<Long> accountOrdersId = orderingDAO.findOrdersIdByUserId(account.getId());
        accountOrdersId.sort(Collections.reverseOrder());
        Map<Ordering, List<CartItem>> accountOrders = new LinkedHashMap<>();
        for (long orderId : accountOrdersId) {
            List<CartItem> ordersCartItems = new ArrayList<>();
            Map<Long, Integer> productAndCount = orderingDAO.findProductAndCountFromOrderByOrderId(orderId);
            productAndCount.forEach((productId, count) -> ordersCartItems.add(new CartItem(productsDAO.findElementById(productId), count)));
            accountOrders.put(orderingDAO.findElementById(orderId), ordersCartItems);
        }

        request.setAttribute("accountOrders", accountOrders);
        return PathConstants.PAGE_ACCOUNT;
    }
}
