package com.dfilatov.istore.web.command.admin;

import com.dfilatov.istore.PathConstants;
import com.dfilatov.istore.web.command.Command;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Redirect to main admin page.
 *
 * @author Denys Filatov
 */
public class AdminCommand extends Command {
    private static final long serialVersionUID = 4116094802510815269L;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        return PathConstants.PAGE_ADMIN;
    }
}
