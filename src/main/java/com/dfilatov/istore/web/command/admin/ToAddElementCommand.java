package com.dfilatov.istore.web.command.admin;

import com.dfilatov.istore.PathConstants;
import com.dfilatov.istore.db.CategoryDAO;
import com.dfilatov.istore.db.ProducerDAO;
import com.dfilatov.istore.db.UserDAO;
import com.dfilatov.istore.db.entity.Category;
import com.dfilatov.istore.db.entity.Producer;
import com.dfilatov.istore.web.command.Command;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Create new entity and send it
 * to add element page.
 *
 * @author Denys Filatov
 */
public class ToAddElementCommand extends Command {
    private static final long serialVersionUID = 5610572931852136272L;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String type = request.getParameter("type");
        if (type.equalsIgnoreCase("user")) {
            UserDAO userDao = new UserDAO();
            List<String> roles = userDao.findAllRoles();
            request.setAttribute("roles", roles);
        } else if (type.equalsIgnoreCase("product")) {
            CategoryDAO categoryDAO = new CategoryDAO();
            List<Category> categories = categoryDAO.getAllElementsWithoutLocalization();
            ProducerDAO producerDAO = new ProducerDAO();
            List<Producer> producers = producerDAO.getAllElements();
            request.setAttribute("categories", categories);
            request.setAttribute("producers", producers);
        }

        request.getSession().setAttribute("element", null);
        return PathConstants.PAGE_UPDATE;
    }
}
