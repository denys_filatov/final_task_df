package com.dfilatov.istore.web.command;

import com.dfilatov.istore.PathConstants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Works when command was not found by web.xml.
 *
 * @author Denys Filatov
 */
public class NoCommand extends Command {
    private static final long serialVersionUID = 6736813803080445645L;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String forward = PathConstants.PAGE_ERROR_PAGE;

        request.setAttribute(PathConstants.ERROR_MESSAGE, "Page not found");
        return forward;
    }
}
