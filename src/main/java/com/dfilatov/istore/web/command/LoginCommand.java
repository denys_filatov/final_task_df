package com.dfilatov.istore.web.command;

import com.dfilatov.istore.db.ProductsDAO;
import com.dfilatov.istore.web.util.CommandUtil;
import com.dfilatov.istore.PathConstants;
import com.dfilatov.istore.db.AccountRole;
import com.dfilatov.istore.db.CategoryDAO;
import com.dfilatov.istore.db.UserDAO;
import com.dfilatov.istore.db.entity.Account;
import com.dfilatov.istore.web.util.SessionUtil;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Get data from login page, validate it
 * and forward to user or admin pages.
 *
 * @author Denys Filatov
 */
public class LoginCommand extends Command {
    private static final long serialVersionUID = -2914110985046846194L;
    private static final Logger LOGGER = Logger.getLogger(LoginCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        LOGGER.debug("Command starts");
        String locale = request.getSession().getAttribute(PathConstants.DEFAULT_LOCALE).toString();

        HttpSession session = request.getSession();

        // obtain login and password from the request
        String login = request.getParameter("login");
        LOGGER.trace("Request parameter: loging --> " + login);

        String password = request.getParameter("password");

        String gRecaptchaResponse = request
                .getParameter("g-recaptcha-response");

        // error handler
        String errorMessage;
        String forward = PathConstants.PAGE_ERROR_PAGE;

        boolean verify = false;
        try {
            verify = CommandUtil.verify(gRecaptchaResponse);
        }catch (IOException e){
            request.setAttribute(PathConstants.ERROR_STRING, e.getMessage());
            return PathConstants.PAGE_LOGIN;
        }

        if (!verify) {
            errorMessage = "You should verify captcha";
            request.setAttribute(PathConstants.ERROR_STRING, errorMessage);
            return PathConstants.PAGE_LOGIN;
        }

        Account account = new UserDAO().findUserByLogin(login);
        LOGGER.trace("Found in DB: account --> " + account);

        if (account.isBlocked()) {
            errorMessage = "Account is blocked";
            request.setAttribute(PathConstants.ERROR_STRING, errorMessage);
            return PathConstants.PAGE_LOGIN;
        } else if (!password.equals(account.getPassword())) {
            errorMessage = "Cannot find account with such login/password";
            request.setAttribute(PathConstants.ERROR_STRING, errorMessage);
            return PathConstants.PAGE_LOGIN;
        } else {
            AccountRole userRole = AccountRole.getRole(account);
            LOGGER.trace("userRole --> " + userRole);

            if (userRole == AccountRole.ADMIN) {
                if (SessionUtil.isCurrentCartIsCreated(request)) {
                    SessionUtil.clearCart(request);
                    LOGGER.debug("Current cart deleted, role is ADMIN");
                }
                forward = PathConstants.ADMIN_COMMAND;
            }

            if (userRole == AccountRole.USER) {

                ProductsDAO productsDAO = new ProductsDAO();
                session.setAttribute("products", productsDAO.findProducts(locale, 12, 0));

                CategoryDAO categoryDAO = new CategoryDAO();
                session.setAttribute("categories", categoryDAO.findCategory(locale));

                forward = PathConstants.COMMAND_LIST_PRODUCTS;
            }

            session.setAttribute("account", account);
            LOGGER.trace("Set the session attribute: account --> " + account);

            session.setAttribute("userRole", userRole);
            LOGGER.trace("Set the session attribute: userRole --> " + userRole);

            LOGGER.info("User " + account + " logged as " + (userRole != null ? userRole.toString().toLowerCase() : null));

        }

        LOGGER.debug("Command finished");
        return forward;
    }

}
