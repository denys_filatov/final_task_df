package com.dfilatov.istore.web.command;

import com.dfilatov.istore.PathConstants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Redirect to log in form.
 *
 * @author Denys Filatov
 */
public class ToLoginCommand extends Command {
    private static final long serialVersionUID = -809527242340335217L;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        return PathConstants.PAGE_LOGIN;
    }
}
