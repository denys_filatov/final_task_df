package com.dfilatov.istore.web.command.user;

import com.dfilatov.istore.PathConstants;
import com.dfilatov.istore.db.ProductsDAO;
import com.dfilatov.istore.db.entity.Product;
import com.dfilatov.istore.exception.IStoreException;
import com.dfilatov.istore.model.Cart;
import com.dfilatov.istore.web.command.Command;
import com.dfilatov.istore.web.util.CommandUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Add product to current shopping cart.
 *
 * @author Denys Filatov
 */
public class AddProductToCartCommand extends Command {
    private static final long serialVersionUID = 4634328886248724042L;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int id = Integer.parseInt(request.getParameter("id"));
        ProductsDAO productsDAO = new ProductsDAO();
        Product product = productsDAO.findElementById(id);
        Cart cart = (Cart) request.getSession().getAttribute(PathConstants.CURRENT_SHOPPING_CART);
        if (product.getCount() > product.getReserved()) {
            try {
                cart.addProduct(product, 1);
            } catch (IStoreException e) {
                String message;
                if (cart.getItems().size() > 49) {
                    message = "Maximum element in cart is 50";
                } else {
                    message = "Maximum count of single product is 5";
                }
                request.setAttribute(PathConstants.ERROR_MESSAGE, message);
                return PathConstants.PAGE_ERROR_PAGE;
            }
            product.setReserved(product.getReserved() + 1);
            productsDAO.updateElement(product);
        }
        if (request.getParameter("query") == null) {
            CommandUtil.getProductsAndCategories(request);
        } else {
            String query = request.getParameter("query");
            CommandUtil.getProductsAndCategories(request, query);
        }
        return PathConstants.COMMAND_LIST_PRODUCTS;
    }
}
