package com.dfilatov.istore.web.command;

import com.dfilatov.istore.web.util.CommandUtil;
import com.dfilatov.istore.PathConstants;
import com.dfilatov.istore.db.entity.Account;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Logging out current user and invalidate session.
 *
 * @author Denys Filatov
 */
public class LogoutCommand extends Command {
    private static final long serialVersionUID = 2968724744348093730L;
    private static final Logger LOGGER = Logger.getLogger(LogoutCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        HttpSession session = request.getSession();

        Account account = (Account) session.getAttribute("account");

        CommandUtil.removeAllReserved(request);

        session.setAttribute("account", null);
        LOGGER.trace("Set the session attribute: account --> " + null);

        session.setAttribute("userRole", null);
        LOGGER.trace("Set the session attribute: userRole --> " + null);

        LOGGER.info("User " + account + " is logged out");

        request.getSession().invalidate();
        LOGGER.debug("SESSION destroyed");
        return PathConstants.COMMAND_LIST_PRODUCTS;
    }
}
