package com.dfilatov.istore.web.command.user;

import com.dfilatov.istore.web.command.Command;
import com.dfilatov.istore.PathConstants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Redirect to shopping cart.
 *
 * @author Denys Filatov
 */
public class ShowCartCommand extends Command {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        return PathConstants.PAGE_CART;
    }
}
