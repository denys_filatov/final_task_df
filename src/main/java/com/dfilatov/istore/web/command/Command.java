package com.dfilatov.istore.web.command;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;

/**
 * Main interface for the Command pattern implementation.
 *
 * @author Denys Filatov
 */
public abstract class Command implements Serializable {
    private static final long serialVersionUID = -5411734077135255728L;
    private static final Logger LOGGER = Logger.getLogger(Command.class);

    /**
     * Execution method for command.
     *
     * @return Address to go once the command is executed.
     */
    public abstract String execute(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException;


    @Override
    public final String toString() {
        return getClass().getSimpleName();
    }
}
