package com.dfilatov.istore.web.command.admin;

import com.dfilatov.istore.PathConstants;
import com.dfilatov.istore.db.*;
import com.dfilatov.istore.db.entity.AbstractEntity;
import com.dfilatov.istore.db.entity.Ordering;
import com.dfilatov.istore.model.CartItem;
import com.dfilatov.istore.web.command.Command;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * To entity management page.
 *
 * @author Denys Filatov
 */
public class RedactorCommand extends Command {
    private static final long serialVersionUID = 8379840305744306510L;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String type = request.getParameter("type");
        List<AbstractEntity> elements = null;
        if (type.equalsIgnoreCase("user")) {
            UserDAO userDao = new UserDAO();
            elements = new ArrayList<>(userDao.getAllElements());
        } else if (type.equalsIgnoreCase("category")) {
            CategoryDAO categoryDAO = new CategoryDAO();
            elements = new ArrayList<>(categoryDAO.getAllElements());
        } else if (type.equalsIgnoreCase("producer")) {
            ProducerDAO producerDAO = new ProducerDAO();
            elements = new ArrayList<>(producerDAO.getAllElements());
        } else if (type.equalsIgnoreCase("product")) {
            String locale = request.getSession().getAttribute(PathConstants.DEFAULT_LOCALE).toString();
            int page = 1;
            int recordPerPage = 6;
            if (request.getParameter("page") != null) {
                page = Integer.parseInt(request.getParameter("page"));
            }
            ProductsDAO productsDAO = new ProductsDAO();
            elements = new ArrayList<>(productsDAO.findProducts(locale, (page - 1) * recordPerPage, recordPerPage));
            int noOfRecords = productsDAO.getNoOfRecords();
            int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordPerPage);
            request.setAttribute("noOfPages", noOfPages);
            request.setAttribute("page", page);
            request.setAttribute("currentCommand", "redactor");
        } else if (type.equalsIgnoreCase("ordering")) {
            OrderingDAO orderingDAO = new OrderingDAO();
            elements = new ArrayList<>(orderingDAO.getAllElements());
            Map<Long, List<CartItem>> productOfOrders = new LinkedHashMap<>();
            ProductsDAO productsDAO = new ProductsDAO();
            elements.forEach(element -> {
                long orderId = ((Ordering) element).getId();
                Map<Long, Integer> productsAndCount = orderingDAO.findProductAndCountFromOrderByOrderId(orderId);
                List<CartItem> products = new ArrayList<>();
                productsAndCount.keySet().forEach(id -> products.add(new CartItem(productsDAO.findElementById(id), productsAndCount.get(id))));
                productOfOrders.put(orderId, products);
            });
            request.setAttribute("productsOfOrder", productOfOrders);
        }

        request.getSession().setAttribute("type", type);
        request.getSession().setAttribute("elements", elements);

        return PathConstants.PAGE_REDACTOR;
    }
}
