package com.dfilatov.istore.web.command.admin;

import com.dfilatov.istore.PathConstants;
import com.dfilatov.istore.db.*;
import com.dfilatov.istore.db.entity.*;
import com.dfilatov.istore.web.command.Command;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Take current element and
 * redirect to updating page.
 *
 * @author Denys Filatov
 */
public class GoToUpdateElementCommand extends Command {
    private static final long serialVersionUID = -2545403799383779544L;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String errorMessage;
        String forward = PathConstants.PAGE_ERROR_PAGE;

        long elementId = Long.parseLong(request.getParameter("id"));
        String type = request.getParameter("type");
        AbstractEntity elementToUpdate = null;
        if (type.equalsIgnoreCase("category")) {
            CategoryDAO categoryDAO = new CategoryDAO();
            List<String> translations = categoryDAO.findCategoryTranslationsById(elementId);
            request.setAttribute("translations", translations);
            elementToUpdate = categoryDAO.findElementById(elementId);
        } else if (type.equalsIgnoreCase("user")) {
            UserDAO userDao = new UserDAO();
            List<String> roles = userDao.findAllRoles();
            request.setAttribute("roles", roles);
            elementToUpdate = userDao.findElementById(elementId);
        } else if (type.equalsIgnoreCase("ordering")) {
            String orderAction = request.getParameter("orderAction");
            OrderingDAO orderingDAO = new OrderingDAO();
            elementToUpdate = orderingDAO.findElementById(elementId);
            if (orderAction.equals("Complete")) {
                Map<Long, Integer> productAndCount = orderingDAO.findProductAndCountFromOrderByOrderId(((Ordering) elementToUpdate).getId());
                ProductsDAO productsDAO = new ProductsDAO();
                for (Map.Entry<Long, Integer> entry : productAndCount.entrySet()) {
                    long productId = entry.getKey();
                    int count = entry.getValue();
                    Product product = productsDAO.findElementById(productId);
                    if (product.getCount() >= count) {
                        product.setCount(product.getCount() - count);
                        product.setReserved(product.getReserved() - count);
                        productsDAO.updateElement(product);
                    } else {
                        errorMessage = "Product " + product.getName() + " not enough at store";
                        request.setAttribute(PathConstants.ERROR_MESSAGE, errorMessage);
                        return forward;
                    }
                }
                orderingDAO.completeOrder((Ordering) elementToUpdate);
            } else if (orderAction.equals("Cancel")) {
                orderingDAO.cancelOrder((Ordering) elementToUpdate);
            }
            return PathConstants.COMMAND_ORDER_REDACTOR;
        } else if (type.equalsIgnoreCase("producer")) {
            ProducerDAO producerDAO = new ProducerDAO();
            elementToUpdate = producerDAO.findElementById(elementId);
        } else if (type.equalsIgnoreCase("product")) {
            ProductsDAO productsDAO = new ProductsDAO();
            CategoryDAO categoryDAO = new CategoryDAO();
            List<Category> categories = categoryDAO.getAllElementsWithoutLocalization();
            ProducerDAO producerDAO = new ProducerDAO();
            List<Producer> producers = producerDAO.getAllElements();
            List<String> nameTranslations = productsDAO.findProductsNameTranslationsById(elementId);
            List<String> descriptionsTranslations = productsDAO.findProductsDescriptionsTranslationsById(elementId);
            request.setAttribute("categories", categories);
            request.setAttribute("producers", producers);
            request.setAttribute("nameTranslations", nameTranslations);
            request.setAttribute("descriptionsTranslations", descriptionsTranslations);
            elementToUpdate = productsDAO.findElementById(elementId);
        }

        request.getSession().setAttribute("element", elementToUpdate);
        return PathConstants.PAGE_UPDATE;
    }
}
