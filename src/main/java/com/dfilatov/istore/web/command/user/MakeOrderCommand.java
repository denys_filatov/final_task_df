package com.dfilatov.istore.web.command.user;

import com.dfilatov.istore.db.OrderStatus;
import com.dfilatov.istore.db.OrderingDAO;
import com.dfilatov.istore.db.entity.Account;
import com.dfilatov.istore.db.entity.Ordering;
import com.dfilatov.istore.model.Cart;
import com.dfilatov.istore.web.command.Command;
import com.dfilatov.istore.PathConstants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Complete user's order.
 *
 * @author Denys Filatov
 */
public class MakeOrderCommand extends Command {
    private static final long serialVersionUID = -1580169056454767956L;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        if (request.getSession().getAttribute("account") == null) {
            request.setAttribute("error", "You must log in before make orders");
            return PathConstants.PAGE_LOGIN;
        }

        Cart cart = (Cart) request.getSession().getAttribute(PathConstants.CURRENT_SHOPPING_CART);

        Account account = (Account) request.getSession().getAttribute("account");

        Ordering ordering = new Ordering();
        ordering.setAccount(account.getName());
        ordering.setOrderStatus(OrderStatus.PENDING.name());
        ordering.setCreated(new Timestamp(System.currentTimeMillis()));

        OrderingDAO orderingDAO = new OrderingDAO();
        orderingDAO.insertElement(ordering);
        long orderId = orderingDAO.getLastInsertedId();

        Map<Long, Integer> productsAndCounts = new LinkedHashMap<>();
        cart.getItems().forEach(k -> productsAndCounts.put(k.getProduct().getId(), k.getCount()));

        orderingDAO.insertOrderItems(orderId, productsAndCounts);

        request.getSession().setAttribute(PathConstants.CURRENT_SHOPPING_CART, new Cart());

        return PathConstants.COMMAND_LIST_PRODUCTS;
    }
}
