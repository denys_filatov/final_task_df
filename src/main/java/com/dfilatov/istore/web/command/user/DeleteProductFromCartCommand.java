package com.dfilatov.istore.web.command.user;

import com.dfilatov.istore.db.ProductsDAO;
import com.dfilatov.istore.db.entity.Product;
import com.dfilatov.istore.model.Cart;
import com.dfilatov.istore.web.command.Command;
import com.dfilatov.istore.PathConstants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Delete product from current cart.
 *
 * @author Denys Filatov
 */
public class DeleteProductFromCartCommand extends Command {
    private static final long serialVersionUID = -340417003601577478L;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        long id = Long.parseLong(request.getParameter("id"));
        ProductsDAO productsDAO = new ProductsDAO();
        Product product = productsDAO.findElementById(id);
        Cart cart = (Cart) request.getSession().getAttribute(PathConstants.CURRENT_SHOPPING_CART);
        cart.removeProduct(id, 1);
        product.setReserved(product.getReserved()-1);
        productsDAO.updateElement(product);
        return PathConstants.PAGE_CART;
    }
}
