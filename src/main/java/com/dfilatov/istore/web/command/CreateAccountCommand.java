package com.dfilatov.istore.web.command;

import com.dfilatov.istore.PathConstants;
import com.dfilatov.istore.db.AccountRole;
import com.dfilatov.istore.db.UserDAO;
import com.dfilatov.istore.db.entity.Account;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Validate data from registration form and
 * create new account.
 *
 * @author Denys Filatov
 */
public class CreateAccountCommand extends Command {
    private static final long serialVersionUID = -2249397241562338669L;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Account account = new Account();
        account.setBlocked(true);
        String firstName = request.getParameter("firstName");
        account.setFirstName(firstName);
        String lastName = request.getParameter("lastName");
        account.setLastName(lastName);
        String name = request.getParameter("name");
        account.setName(name);
        String password = request.getParameter("password");
        account.setPassword(password);
        account.setRole(AccountRole.USER.getName());
        UserDAO userDAO = new UserDAO();
        if(userDAO.findUserByLogin(account.getName()) == null) {
            userDAO.insertElement(account);
        } else {
            request.setAttribute("error", "User with this nickname is already exist!");
            return PathConstants.PAGE_CREATE_ACCOUNT;
        }
        return PathConstants.COMMAND_LIST_PRODUCTS;
    }
}
