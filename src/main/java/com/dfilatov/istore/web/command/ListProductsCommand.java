package com.dfilatov.istore.web.command;

import com.dfilatov.istore.PathConstants;
import com.dfilatov.istore.web.util.CommandUtil;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Get list of product and categories from database
 * and set it to request.
 * Main Page command.
 *
 * @author Denys Filatov
 */
public class ListProductsCommand extends Command {

    private static final long serialVersionUID = -8304386290486079553L;
    private static final Logger LOGGER = Logger.getLogger(ListProductsCommand.class);

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        LOGGER.debug("Command starts");
        CommandUtil.getProductsAndCategories(request);

        request.setAttribute("currentCommand", "listProducts");

        LOGGER.debug("Command finished");
        return PathConstants.PAGE_PRODUCTS_LIST;
    }

}
