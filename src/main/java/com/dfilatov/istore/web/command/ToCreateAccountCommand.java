package com.dfilatov.istore.web.command;

import com.dfilatov.istore.PathConstants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Redirect to registration form.
 *
 * @author Denys Filatov
 */
public class ToCreateAccountCommand extends Command{
    private static final long serialVersionUID = -8695202712392505652L;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        return PathConstants.PAGE_CREATE_ACCOUNT;
    }
}
