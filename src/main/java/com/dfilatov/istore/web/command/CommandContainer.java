package com.dfilatov.istore.web.command;

import com.dfilatov.istore.web.command.admin.*;
import com.dfilatov.istore.web.command.user.*;
import org.apache.log4j.Logger;

import java.util.Map;
import java.util.TreeMap;

/**
 * Holder for all commands.
 *
 * @author Denys Filatov
 */

public class CommandContainer {
    private static final Logger log = Logger.getLogger(CommandContainer.class);

    private static final Map<String, Command> commands = new TreeMap<>();

    static {
        // common commands
        commands.put("login", new LoginCommand());
        commands.put("Logout", new LogoutCommand());
        commands.put("noCommand", new NoCommand());
        commands.put("toLogin", new ToLoginCommand());
        commands.put("changeLanguage", new ChangeLanguageCommand());
        commands.put("orderedListProducts", new OrderedListProductsCommand());
        commands.put("showCart", new ShowCartCommand());
        commands.put("addProductToCard", new AddProductToCartCommand());
        commands.put("deleteFromCart", new DeleteProductFromCartCommand());
        commands.put("makeOrder", new MakeOrderCommand());
        commands.put("toCreateAccount", new ToCreateAccountCommand());
        commands.put("createAccount", new CreateAccountCommand());

        // client commands
        commands.put("listProducts", new ListProductsCommand());
        commands.put("toAccount", new ToAccountCommand());
        commands.put("cancelOrder", new CancelOrderCommand());

        // admin commands
        commands.put("adminCommand", new AdminCommand());
        commands.put("redactor", new RedactorCommand());
        commands.put("goToUpdateElement", new GoToUpdateElementCommand());
        commands.put("updateElement", new UpdateElementCommand());
        commands.put("deleteElement", new DeleteElementCommand());
        commands.put("toAddElement", new ToAddElementCommand());
        commands.put("addElement", new AddElementCommand());

        log.debug("Command container was successfully initialized");
        log.trace("Number of commands --> " + commands.size());
    }

    /**
     * Returns command object with the given name.
     *
     * @param commandName Name of the command.
     * @return Command object.
     */
    public static Command get(String commandName) {
        if (commandName == null || !commands.containsKey(commandName)) {
            log.trace("Command not found, name --> " + commandName);
            return commands.get("noCommand");
        }

        return commands.get(commandName);
    }

}
