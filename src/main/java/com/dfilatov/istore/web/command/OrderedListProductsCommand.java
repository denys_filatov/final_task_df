package com.dfilatov.istore.web.command;

import com.dfilatov.istore.web.util.CommandUtil;
import com.dfilatov.istore.PathConstants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Gets order rules from request,
 * and finds ordered products.
 *
 * @author Denys Filatov
 */
public class OrderedListProductsCommand extends Command {
    private static final long serialVersionUID = -2028637972982088235L;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String query = CommandUtil.getRequestQuery(request);
        CommandUtil.getProductsAndCategories(request, query);
        request.setAttribute("currentCommand", "orderedListProducts");
        return PathConstants.PAGE_PRODUCTS_LIST;
    }
}
