package com.dfilatov.istore.web.command.user;

import com.dfilatov.istore.db.OrderingDAO;
import com.dfilatov.istore.web.command.Command;
import com.dfilatov.istore.PathConstants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Cancel order by user.
 *
 * @author Denys Filatov
 */
public class CancelOrderCommand extends Command {
    private static final long serialVersionUID = -6909967861072186231L;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        long orderId = Long.parseLong(request.getParameter("id"));
        OrderingDAO orderingDAO = new OrderingDAO();
        orderingDAO.deleteOrderItemsByOrderId(orderId);
        orderingDAO.deleteElementById(orderId);
        return PathConstants.COMMAND_ACCOUNT;
    }
}
