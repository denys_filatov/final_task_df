package com.dfilatov.istore.web.command.admin;

import com.dfilatov.istore.PathConstants;
import com.dfilatov.istore.db.*;
import com.dfilatov.istore.db.entity.*;
import com.dfilatov.istore.web.command.Command;
import com.dfilatov.istore.web.util.CommandUtil;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Takes parameters from request
 * and set it to entity.
 * Update element in database.
 *
 * @author Denys Filatov
 */
public class UpdateElementCommand extends Command {
    private static final long serialVersionUID = -4555704595229565867L;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        long elementId = Long.parseLong(request.getParameter("id"));
        String type = request.getParameter("type");
        List<AbstractEntity> elements = null;
        if (type.equalsIgnoreCase("category")) {
            elements = new ArrayList<>(updateCategory(request, elementId));
        } else if (type.equalsIgnoreCase("user")) {
            elements = new ArrayList<>(updateAccount(request, elementId));
        } else if (type.equalsIgnoreCase("producer")) {
            elements = new ArrayList<>(updateProducer(request, elementId));
        } else if (type.equalsIgnoreCase("product")) {
            elements = new ArrayList<>(updateProduct(request, elementId));
        }
        request.getSession().setAttribute("type", type);
        request.getSession().setAttribute("elements", elements);

        return PathConstants.PAGE_REDACTOR;
    }

    private List<Product> updateProduct(HttpServletRequest request, long elementId) {
        ProductsDAO productsDAO = new ProductsDAO();
        Product product = productsDAO.findElementById(elementId);
        CommandUtil.updateProductById(request, product);
        List<String> productNameTranslations = CommandUtil.getProductNameTranslation(request);
        List<String> productDescriptionTranslations = CommandUtil.getProductDescriptionTranslation(request);
        productsDAO.updateElement(product);

        CategoryDAO categoryDAO = new CategoryDAO();
        categoryDAO.updateElementsCount(categoryDAO.getAllElements());

        ProducerDAO producerDAO = new ProducerDAO();
        producerDAO.updateElementsCount(producerDAO.getAllElements());

        productsDAO.updateElementNameTranslations(elementId, productNameTranslations);
        productsDAO.updateElementDescriptionTranslations(elementId, productDescriptionTranslations);
        return productsDAO.getAllElements();
    }

    private List<Producer> updateProducer(HttpServletRequest request, long elementId) {
        ProducerDAO producerDAO = new ProducerDAO();
        Producer producer = producerDAO.findElementById(elementId);
        String producerName = request.getParameter("producerName");
        producer.setName(producerName);
        producerDAO.updateElement(producer);
        producerDAO.updateSingleElementCount(producer);
        return producerDAO.getAllElements();
    }

    private List<Category> updateCategory(HttpServletRequest request, long elementId) {
        CategoryDAO categoryDAO = new CategoryDAO();
        Category category = categoryDAO.findElementById(elementId);
        String categoryName = request.getParameter("categoryName");
        category.setName(categoryName);
        List<String> translations = new ArrayList<>();
        String categoryUaTranslation = request.getParameter("categoryUaTranslation");
        translations.add(categoryUaTranslation);
        String categoryEnTranslation = request.getParameter("categoryEnTranslation");
        translations.add(categoryEnTranslation);
        categoryDAO.updateElement(category);
        categoryDAO.updateElement(category);
        categoryDAO.updateElementTranslations(elementId, translations);
        return categoryDAO.getAllElements();
    }

    private List<Account> updateAccount(HttpServletRequest request, long id) {
        UserDAO userDao = new UserDAO();
        Account account = userDao.findElementById(id);
        String name = request.getParameter("name");
        account.setName(name);
        String password = request.getParameter("password");
        account.setPassword(password);
        String role = request.getParameter("role");
        AccountRole accountRole = AccountRole.getRoleByName(role);
        if (accountRole != null) {
            account.setRole(accountRole.getName());
        }
        String blocked = request.getParameter("blocked");
        if (blocked != null) {
            if (blocked.equals("blocked")) {
                account.setBlocked(true);
            } else if (blocked.equals("unblocked")) {
                account.setBlocked(false);
            }
        }
        String firstName = request.getParameter("firstName");
        account.setFirstName(firstName);
        String lastName = request.getParameter("lastName");
        account.setLastName(lastName);
        userDao.updateElement(account);
        return userDao.getAllElements();
    }

}
