package com.dfilatov.istore.web.command.admin;

import com.dfilatov.istore.PathConstants;
import com.dfilatov.istore.db.*;
import com.dfilatov.istore.db.entity.*;
import com.dfilatov.istore.web.command.Command;
import com.dfilatov.istore.web.util.CommandUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Create new entity and add it to database.
 * Type of entity sends from request.
 *
 * @author Denys Filatov
 */
public class AddElementCommand extends Command {
    private static final long serialVersionUID = 5610572931852136272L;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String type = request.getParameter("type");
        List<AbstractEntity> elements = null;
        if (type.equalsIgnoreCase("user")) {
            elements = new ArrayList<>(addAccount(request));
        } else if (type.equalsIgnoreCase("category")) {
            elements = new ArrayList<>(addCategory(request));
        } else if (type.equalsIgnoreCase("producer")) {
            elements = new ArrayList<>(addProducer(request));
        } else if (type.equalsIgnoreCase("product")) {
            elements = new ArrayList<>(addProduct(request));
        }
        request.getSession().setAttribute("type", type);
        request.getSession().setAttribute("elements", elements);

        return PathConstants.PAGE_REDACTOR;
    }

    private List<Account> addAccount(HttpServletRequest request) {
        UserDAO userDAO = new UserDAO();
        Account account = new Account();
        String name = request.getParameter("name");
        String password = request.getParameter("password");
        String role = request.getParameter("role");
        String blocked = request.getParameter("blocked");
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        account.setName(name);
        account.setPassword(password);
        account.setRole(Objects.requireNonNull(AccountRole.getRoleByName(role)).getName());
        if (blocked != null) {
            account.setBlocked(blocked.equals("blocked"));
        }
        account.setFirstName(firstName);
        account.setLastName(lastName);
        userDAO.insertElement(account);
        return userDAO.getAllElements();
    }

    private List<Category> addCategory(HttpServletRequest request) {
        CategoryDAO categoryDAO = new CategoryDAO();
        Category category = new Category();
        String categoryName = request.getParameter("categoryName");
        category.setName(categoryName);
        List<String> categoryTranslations = new ArrayList<>();
        String categoryUaTranslation = request.getParameter("categoryUaTranslation");
        categoryTranslations.add(categoryUaTranslation);
        String categoryEnTranslation = request.getParameter("categoryEnTranslation");
        categoryTranslations.add(categoryEnTranslation);

        categoryDAO.insertElement(category);

        int categoryId = categoryDAO.findCategoryIdByName(categoryName);
        categoryDAO.insertCategoryTranslations(categoryId, categoryTranslations);

        return categoryDAO.getAllElements();
    }

    private List<Producer> addProducer(HttpServletRequest request) {
        ProducerDAO producerDAO = new ProducerDAO();
        Producer producer = new Producer();
        String producerName = request.getParameter("producerName");
        producer.setName(producerName);
        producerDAO.insertElement(producer);

        return producerDAO.getAllElements();
    }

    private List<Product> addProduct(HttpServletRequest request) {
        ProductsDAO productDAO = new ProductsDAO();
        Product product = new Product();
        product.setId(0L);
        CommandUtil.updateProductById(request, product);

        List<String> productNameTranslations = CommandUtil.getProductNameTranslation(request);

        List<String> productDescriptionTranslations = CommandUtil.getProductDescriptionTranslation(request);

        productDAO.insertElement(product);

        CategoryDAO categoryDAO = new CategoryDAO();
        categoryDAO.updateElementsCount(categoryDAO.getAllElements());

        ProducerDAO producerDAO = new ProducerDAO();
        producerDAO.updateElementsCount(producerDAO.getAllElements());

        int id = productDAO.findElementIdByName(product.getName());
        productDAO.insertTranslationsByProductId(id, productNameTranslations, productDescriptionTranslations);

        return productDAO.getAllElements();
    }

}
