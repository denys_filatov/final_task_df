package com.dfilatov.istore.web.command;

import com.dfilatov.istore.web.util.CommandUtil;
import com.dfilatov.istore.PathConstants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;
import java.io.IOException;

/**
 * Get needed locale from request
 * and change session locale.
 *
 * @author Denys Filatov
 */
public class ChangeLanguageCommand extends Command {
    private static final long serialVersionUID = -4483170252106053768L;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String localeToSet = request.getParameter("localeToSet");
        if (localeToSet != null && !localeToSet.isEmpty()) {
            HttpSession session = request.getSession();
            Config.set(session, "javax.servlet.jsp.jstl.fmt.locale", localeToSet);
            session.setAttribute(PathConstants.DEFAULT_LOCALE, localeToSet);
        }
        CommandUtil.getProductsAndCategories(request);
        return PathConstants.COMMAND_LIST_PRODUCTS;
    }
}
