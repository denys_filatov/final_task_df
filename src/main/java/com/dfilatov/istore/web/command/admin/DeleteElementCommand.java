package com.dfilatov.istore.web.command.admin;

import com.dfilatov.istore.PathConstants;
import com.dfilatov.istore.db.*;
import com.dfilatov.istore.db.entity.AbstractEntity;
import com.dfilatov.istore.web.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Delete element from database.
 *
 * @author Denys Filatov
 */
public class DeleteElementCommand extends Command {
    private static final Logger LOGGER = Logger.getLogger(DeleteElementCommand.class);
    private static final long serialVersionUID = -1789244353634443699L;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String errorMessage;
        String forward = PathConstants.PAGE_ERROR_PAGE;

        long elementId = Long.parseLong(request.getParameter("id"));
        String type = request.getParameter("type");
        List<AbstractEntity> elements = null;
        if (type.equalsIgnoreCase("category")) {
            CategoryDAO categoryDAO = new CategoryDAO();
            categoryDAO.deleteCategoryTranslationsById(elementId);
            categoryDAO.deleteElementById(elementId);
            elements = new ArrayList<>(categoryDAO.getAllElements());
        } else if (type.equalsIgnoreCase("user")) {
            UserDAO userDao = new UserDAO();
            int countOfOrders = userDao.findUserOrdersCountByID(elementId);
            if (countOfOrders == 0) {
                userDao.deleteElementById(elementId);
            } else {
                errorMessage = "User with id - " + elementId + " has open orders";
                request.setAttribute(PathConstants.ERROR_MESSAGE, errorMessage);
                LOGGER.error("errorMessage --> " + errorMessage);
                return forward;
            }
            elements = new ArrayList<>(userDao.getAllElements());
        } else if (type.equalsIgnoreCase("ordering")) {
            OrderingDAO orderingDAO = new OrderingDAO();
            orderingDAO.deleteElementById(elementId);
            elements = new ArrayList<>(orderingDAO.getAllElements());
        } else if (type.equalsIgnoreCase("producer")) {
            ProducerDAO producerDAO = new ProducerDAO();
            int countOfProducerProducts = producerDAO.findCountOfProducerProducts(elementId);
            if (countOfProducerProducts == 0) {
                producerDAO.deleteElementById(elementId);
            } else {
                errorMessage = "In database exist products of producer with id - " + elementId;
                request.setAttribute(PathConstants.ERROR_MESSAGE, errorMessage);
                LOGGER.error("errorMessage --> " + errorMessage);
                return forward;
            }
            elements = new ArrayList<>(producerDAO.getAllElements());
        } else if (type.equalsIgnoreCase("product")) {
            ProductsDAO productsDAO = new ProductsDAO();
            productsDAO.deleteProductTranslationsById(elementId);
            productsDAO.deleteElementById(elementId);

            CategoryDAO categoryDAO = new CategoryDAO();
            categoryDAO.updateElementsCount(categoryDAO.getAllElements());

            ProducerDAO producerDAO = new ProducerDAO();
            producerDAO.updateElementsCount(producerDAO.getAllElements());

            elements = new ArrayList<>(productsDAO.getAllElements());
        }

        request.getSession().setAttribute("type", type);
        request.getSession().setAttribute("elements", elements);

        return PathConstants.PAGE_REDACTOR;
    }
}
