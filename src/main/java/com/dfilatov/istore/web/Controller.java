package com.dfilatov.istore.web;

import com.dfilatov.istore.web.command.Command;
import com.dfilatov.istore.web.command.CommandContainer;
import com.dfilatov.istore.PathConstants;
import com.dfilatov.istore.db.entity.Account;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Main servlet controller.
 *
 * @author Denys Filatov
 */
public class Controller extends HttpServlet {

    private static final long serialVersionUID = -6615076878525836237L;

    private static final Logger LOGGER = Logger.getLogger(Controller.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getCharacterEncoding() == null) {
            req.setCharacterEncoding("UTF-8");
        }
        process(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getCharacterEncoding() == null) {
            req.setCharacterEncoding("UTF-8");
        }
        process(req, resp);
    }

    /**
     * Main method of this controller
     */
    private void process(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.debug("Controller starts");
        Account account = (Account) req.getSession().getAttribute("account");

        String locale;
        try {
            locale = req.getSession().getAttribute(PathConstants.DEFAULT_LOCALE).toString();
        } catch (NullPointerException e) {
            locale = req.getServletContext().getAttribute(PathConstants.DEFAULT_LOCALE).toString();
        }
        req.getSession().setAttribute(PathConstants.DEFAULT_LOCALE, locale);

        String commandName = req.getParameter("command");
        LOGGER.debug("Request parameter: command -> " + commandName);

        Command command = CommandContainer.get(commandName);
        LOGGER.debug("Obtained command --> " + command);

        String forward = command.execute(req, resp);
        LOGGER.trace("Forward address --> " + forward);

        LOGGER.debug("Controller finished, now go to forward address --> " + forward);

        if (forward != null) {
            if (forward.startsWith("/controller")) {
                resp.sendRedirect(forward);
            } else if (forward.startsWith("/WEB-INF")){
                req.getRequestDispatcher(forward).forward(req, resp);
            } else {
                req.setAttribute(PathConstants.CURRENT_PAGE, "page/" + forward);
                if(account != null && account.getRole() != null) {
                    if (account.getRole().equalsIgnoreCase("admin")) {
                        req.getRequestDispatcher(PathConstants.PAGE_ADMIN).forward(req, resp);
                        return;
                    }
                }
                req.getRequestDispatcher(PathConstants.PAGE_PAGE_TEMPLATE).forward(req, resp);
            }
        }
    }


}
