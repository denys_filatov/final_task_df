<%@ tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="categories" required="true" type="java.util.Collection" %>

<div class="list-group">
    <c:forEach var="category" items="${categories}">
        <c:choose>
            <c:when test="${categoryName == category.name}">
                <a style="background: #c7ddef"
                   href="controller?command=orderedListProducts&categoryName=${category.name}" class="list-group-item">${category.name}</a>
            </c:when>
            <c:otherwise>
                <a href="controller?command=orderedListProducts&categoryName=${category.name}" class="list-group-item">${category.name}</a>
            </c:otherwise>
        </c:choose>
    </c:forEach>
</div>
