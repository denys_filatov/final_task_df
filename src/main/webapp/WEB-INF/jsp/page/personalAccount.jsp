<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="cart" uri="https://tag.CartAmountTag" %>
<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" trimDirectiveWhitespaces="true" %>
<!DOCTYPE html>
<html>
<head>
    <title><fmt:message key="personal_account_title"/></title>
    <link href="/static/css/bootstrap.css" rel="stylesheet">
    <link href="/static/css/bootstrap-theme.css" rel="stylesheet">
    <link href="/static/css/font-awesome.css" rel="stylesheet">
    <link href="/static/css/app.css" rel="stylesheet">
</head>
<body>
<header>
    <jsp:include page="../fragment/header.jsp"/>
</header>
<div class="container-fluid">
    <div class="row">
        <main>
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th class="text-center" scope="col">Order ID</th>
                    <th class="text-center" scope="col">Products</th>
                    <th class="text-center" scope="col">Status</th>
                    <th class="text-center" scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="entry" items="${accountOrders}">
                    <tr>
                        <td class="text-center">${entry.key.id}</td>
                        <td class="text-center">
                            <ul>
                                <c:forEach var="cartItem" items="${entry.value}">
                                    <li>${cartItem.product.name} - ${cartItem.count} items.</li>
                                </c:forEach>
                            </ul>
                        </td>
                        <td class="text-center">${entry.key.orderStatus}</td>
                        <td class="text-center">
                            <c:if test="${entry.key.orderStatus == 'PENDING'}">
                                <div class="btn-group">
                                    <form method="post" action="controller">
                                        <input type="hidden" name="command" value="cancelOrder">
                                        <input type="hidden" name="id" value="${entry.key.id}">
                                        <button class="btn btn-primary" type="submit">Cancel</button>
                                    </form>
                                </div>
                            </c:if>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
                <tfoot>
                <tr>
                    <th class="text-center" scope="col">Order ID</th>
                    <th class="text-center" scope="col">Products</th>
                    <th class="text-center" scope="col">Status</th>
                    <th class="text-center" scope="col">Actions</th>
                </tr>
                </tfoot>
            </table>
        </main>
    </div>
</div>
<footer class="footer">
    <jsp:include page="../fragment/footer.jsp"/>
</footer>
</body>
</html>