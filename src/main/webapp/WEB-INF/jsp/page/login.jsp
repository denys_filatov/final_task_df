<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title><fmt:message key="login_title"/></title>
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <script>
        window.onload = function () {
            var recaptcha = document.forms["logInForm"]["g-recaptcha"];
            recaptcha.required = true;
            recaptcha.oninvalid = function (e) {
                // do something
                alert("Please complete the captcha");
            }
        }
    </script>
    <link href="/static/css/bootstrap.css" rel="stylesheet">
    <link href="/static/css/bootstrap-theme.css" rel="stylesheet">
    <link href="/static/css/font-awesome.css" rel="stylesheet">
    <link href="/static/css/app.css" rel="stylesheet">
</head>

<c:set var="title" value="Login"/>
<body>
<c:if test="${not empty error}">
    <div class="alert alert-danger hidden-print" role="alert">
        ${error}
    </div>
</c:if>
<div class="login-form">
    <form action="controller" method="post" id="logInForm">
        <input type="hidden" name="command" value="login"/>
        <h2 class="text-center"><fmt:message key="login_login"/></h2>
        <div class="form-group">
            <input type="text" name="login" class="form-control" placeholder="<fmt:message key="login_username"/>"
                   required>
        </div>
        <div class="form-group">
            <input type="password" name="password" class="form-control"
                   placeholder="<fmt:message key="login_password"/>" required>
        </div>
        <div class="g-recaptcha" id="g-recaptcha"
             data-sitekey="6LcH1dcZAAAAAGN9M1bSHdL0NO4j6arpgbVlm34v">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block"><fmt:message key="login_sign_in"/></button>
        </div>

    </form>
    <p class="text-center"><a href="/controller?command=toCreateAccount"><fmt:message key="login_create_account"/></a>
    </p>
</div>
</body>
</html>