<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" trimDirectiveWhitespaces="true" %>

<table id="dtBasicExample" class="table table-striped table-bordered " cellspacing="0" width="100%">
    <thead>
    <tr>
        <c:if test="${type != 'ordering'}">
            <form method="post" action="controller">
                <input type="hidden" name="command" value="toAddElement">
                <input type="hidden" name="type" value=${type}>
                <button class="btn btn-primary" type="submit">Add New</button>
            </form>
        </c:if>
    </tr>
    <tr>
        <c:if test="${type == 'user'}">
            <th class="th-sm">Nickname</th>
            <th class="th-sm">First name</th>
            <th class="th-sm">Last name</th>
            <th class="th-sm">Role</th>
            <th class="th-sm">Status</th>
            <th class="th-sm">Actions</th>
        </c:if>
        <c:if test="${type == 'category'}">
            <th class="th-sm">Name</th>
            <th class="th-sm">Count</th>
            <th class="th-sm">Actions</th>
        </c:if>
        <c:if test="${type == 'producer'}">
            <th class="th-sm">Name</th>
            <th class="th-sm">Count</th>
            <th class="th-sm">Actions</th>
        </c:if>
        <c:if test="${type == 'product'}">
            <th class="th-sm">Name</th>
            <th class="th-sm">Price</th>
            <th class="th-sm">Category</th>
            <th class="th-sm">Producer</th>
            <th class="th-sm">Made Date</th>
            <th class="th-sm">Actions</th>
        </c:if>
        <c:if test="${type == 'ordering'}">
            <th class="th-sm">Account</th>
            <th class="th-sm">Created</th>
            <th class="th-sm">Elements</th>
            <th class="th-sm">Status</th>
            <th class="th-sm">Actions</th>
        </c:if>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="element" items="${elements}">
        <tr>
            <c:if test="${type == 'user'}">
                <td>${element.name}</td>
                <td>${element.firstName}</td>
                <td>${element.lastName}</td>
                <td>${element.role}</td>
                <c:choose>
                    <c:when test="${element.blocked == true}">
                        <td>BLOCKED</td>
                    </c:when>
                    <c:otherwise>
                        <td>UNBLOCKED</td>
                    </c:otherwise>
                </c:choose>
            </c:if>
            <c:if test="${type == 'category'}">
                <td>${element.name}</td>
                <td>${element.count}</td>
            </c:if>
            <c:if test="${type == 'producer'}">
                <td>${element.name}</td>
                <td>${element.count}</td>
            </c:if>
            <c:if test="${type == 'product'}">
                <td>${element.name}</td>
                <td>${element.price}</td>
                <td>${element.category}</td>
                <td>${element.producer}</td>
                <td>${element.madeDate}</td>
            </c:if>
            <c:if test="${type == 'ordering'}">
                <td>${element.account}</td>
                <td>${element.created}</td>
                <td>
                    <c:forEach var="cartItem" items="${productsOfOrder[element.id]}">
                        <ul>
                            <li>${cartItem.product.name} : ${cartItem.count} items.</li>
                        </ul>
                    </c:forEach>
                </td>
                <td>${element.orderStatus}</td>
            </c:if>
            <c:choose>
                <c:when test="${type == 'ordering'}">
                    <td>
                        <div class="btn-group">
                            <form method="post" action="controller">
                                <input type="hidden" name="command" value="goToUpdateElement">
                                <input type="hidden" name="id" value=${element.id}>
                                <input type="hidden" name="type" value=${type}>
                                <c:if test="${element.orderStatus == 'PENDING'}">
                                    <input name="orderAction" class="btn btn-primary" type="submit" value="Complete"/>
                                    <input name="orderAction" class="btn btn-primary" type="submit" value="Cancel"/>
                                </c:if>
                            </form>
                        </div>
                    </td>
                </c:when>
                <c:otherwise>
                    <td>
                        <div class="btn-group">
                            <form method="post" action="controller">
                                <input type="hidden" name="command" value="goToUpdateElement">
                                <input type="hidden" name="id" value=${element.id}>
                                <input type="hidden" name="type" value=${type}>
                                <button class="btn btn-primary" type="submit">Update</button>
                            </form>
                            <form method="post" action="controller">
                                <input type="hidden" name="command" value="deleteElement">
                                <input type="hidden" name="id" value=${element.id}>
                                <input type="hidden" name="type" value=${type}>
                                <button class="btn btn-primary" type="submit">Delete</button>
                            </form>
                        </div>
                    </td>

                </c:otherwise>
            </c:choose>
        </tr>
    </c:forEach>
    </tbody>
    <tfoot>
    <tr>
        <c:if test="${type == 'user'}">
            <th class="th-sm">Nickname</th>
            <th class="th-sm">First Name</th>
            <th class="th-sm">Last Name</th>
            <th class="th-sm">Role</th>
            <th class="th-sm">Actions</th>
        </c:if>
        <c:if test="${type == 'category'}">
            <th class="th-sm">Name</th>
            <th class="th-sm">Count</th>
            <th class="th-sm">Actions</th>
        </c:if>
        <c:if test="${type == 'producer'}">
            <th class="th-sm">Name</th>
            <th class="th-sm">Count</th>
            <th class="th-sm">Actions</th>
        </c:if>
        <c:if test="${type == 'product'}">
            <th class="th-sm">Name</th>
            <th class="th-sm">Price</th>
            <th class="th-sm">Category</th>
            <th class="th-sm">Producer</th>
            <th class="th-sm">Made Date</th>
            <th class="th-sm">Actions</th>
        </c:if>
        <c:if test="${type == 'ordering'}">
            <th class="th-sm">Account</th>
            <th class="th-sm">Created</th>
            <th class="th-sm">Elements</th>
            <th class="th-sm">Status</th>
            <th class="th-sm">Actions</th>
        </c:if>
    </tr>
    </tfoot>
</table>
<c:if test="${type == 'product'}">
    <div class="text-center">
        <ul class="pagination">
            <c:if test="${page != 1}">
                <li><a href="controller?command=${currentCommand}&type=product&page=${page - 1}${query}">Previous</a></li>
            </c:if>
            <c:forEach begin="1" end="${noOfPages}" var="i">
                <c:choose>
                    <c:when test="${page eq i}">
                        <li><a>${i}</a></li>
                    </c:when>
                    <c:otherwise>
                        <li><a href="controller?command=${currentCommand}&type=product&page=${i}${query}">${i}</a></li>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
            <c:if test="${page lt noOfPages}">
                <li><a href="/controller?command=${currentCommand}&type=product&page=${page+1}${query}">Next</a></li>
            </c:if>
        </ul>
    </div>
</c:if>
