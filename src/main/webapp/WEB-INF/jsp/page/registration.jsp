<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><fmt:message key="registration_title"/></title>
    <link href="/static/css/bootstrap.css" rel="stylesheet">
    <link href="/static/css/bootstrap-theme.css" rel="stylesheet">
    <link href="/static/css/font-awesome.css" rel="stylesheet">
    <link href="/static/css/app.css" rel="stylesheet">
</head>
<body>
<c:if test="${not empty error}">
    <div class="alert alert-danger hidden-print" role="alert">
        User is already exist.
    </div>
</c:if>

<div class="container">
    <div class="row centered-form">
        <div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><fmt:message key="registration_sign_up"/></h3>
                </div>
                <div class="panel-body">
                    <form role="form" action="controller" method="post">
                        <input type="hidden" name="command" value="createAccount">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="text" name="firstName" pattern="\p{L}{2,}" id="first_name"
                                           class="form-control input-sm"
                                           placeholder="<fmt:message key="registration_first_name"/>" required>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="text" name="lastName" pattern="\p{L}{2,}" id="last_name"
                                           class="form-control input-sm"
                                           placeholder="<fmt:message key="registration_last_name"/>" required>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <input type="text" name="name" id="nickname" pattern="\p{L}{2,}"
                                   class="form-control input-sm"
                                   placeholder="<fmt:message key="registration_nickname"/>" required>
                        </div>

                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="password" name="password" pattern=".{6,}" title="6 and more symbols"
                                           id="password" class="form-control input-sm"
                                           placeholder="<fmt:message key="registration_password"/>" required>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="password" name="password_confirmation" pattern=".{6,}"
                                           title="6 and more symbols" id="password_confirmation"
                                           class="form-control input-sm"
                                           placeholder="<fmt:message key="registration_confirm_password"/>" required>
                                </div>
                            </div>
                        </div>

                        <input type="submit" value="<fmt:message key="registration_register"/>"
                               class="btn btn-info btn-block" onclick="return Validate()">

                    </form>
                    <script type="text/javascript">
                        function Validate() {
                            var password = document.getElementById("password").value;
                            var confirmPassword = document.getElementById("password_confirmation").value;
                            if (password != confirmPassword) {
                                alert("Passwords do not match.");
                                return false;
                            }
                            return true;
                        }
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
