<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<form class="form-inline">

    <c:if test="${type == 'category'}">
        <jsp:include page="../fragment/category_cru.jsp"></jsp:include>
    </c:if>
    <c:if test="${type == 'user'}">
        <jsp:include page="../fragment/user_cru.jsp"></jsp:include>
    </c:if>
    <c:if test="${type == 'producer'}">
        <jsp:include page="../fragment/producer_cru.jsp"></jsp:include>
    </c:if>
    <c:if test="${type == 'ordering'}">
        <jsp:include page="../fragment/ordering_cru.jsp"></jsp:include>
    </c:if>
    <c:if test="${type == 'product'}">
        <jsp:include page="../fragment/product_cru.jsp"></jsp:include>
    </c:if>
    <c:if test="${element != null}">
        <form class="form-inline" method="post" action="controller">
            <input type="hidden" name="command" value="updateElement"/>
            <input type="hidden" name="id" value="${element.id}"/>
            <button type="submit" class="btn btn-primary mb-2">Update</button>
        </form>
    </c:if>
    <c:if test="${element == null}">
        <form class="form-inline" method="post" action="controller">
            <input type="hidden" name="command" value="addElement"/>
            <button type="submit" class="btn btn-primary mb-2">Add</button>
        </form>
    </c:if>

</form>