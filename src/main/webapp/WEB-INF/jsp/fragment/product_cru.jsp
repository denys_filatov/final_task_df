<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<input type="hidden" name="type" value="product"/>
<div class="form-group">
    <legend>
        <h6>
            Name
        </h6>
    </legend>
    <input type="text" name="productName" class="form-control" value="${element.name}" required>
</div>
<div class="form-group">
    <legend>
        <h6>
            Ua Name
        </h6>
    </legend>
    <input type="text" name="productUaName" class="form-control" value="${nameTranslations[0]}" required>
</div>
<div class="form-group">
    <legend>
        <h6>
            En Name
        </h6>
    </legend>
    <input type="text" name="productEnName" class="form-control" value="${nameTranslations[1]}" required>
</div>
<div class="form-group">
    <legend>
        <h6>
            Count
        </h6>
    </legend>
    <input type="number" name="productCount" class="form-control" min="1" value="${element.count}" required>
</div>
<div class="form-group">
    <legend>
        <h6>
            Image Link
        </h6>
    </legend>
    <input type="text" name="productImageLink" class="form-control" value="${element.imageLink}" required>
</div>
<div class="form-group">
    <legend>
        <h6>
            Price
        </h6>
    </legend>
    <input type="number" name="productPrice" class="form-control" min="1" value="${element.price}" required>
</div>
<div class="form-group">
    <legend>
        <h6>
            Category
        </h6>
    </legend>
    <select name="productCategory">
        <option selected="selected">${element.category}</option>
        <c:forEach var="category" items="${categories}">
            <c:if test="${category.name != element.category}">
                <option>${category.name}</option>
            </c:if>
        </c:forEach>
    </select>
</div>
<div class="form-group">
    <legend>
        <h6>
            Producer
        </h6>
    </legend>
    <select name="productProducer">
        <option selected="selected">${element.producer}</option>
        <c:forEach var="producer" items="${producers}">
            <c:if test="${producer.name != element.producer}">
                <option>${producer.name}</option>
            </c:if>
        </c:forEach>
    </select>
</div>
<div class="form-group">
    <legend>
        <h6>
            Made Date
        </h6>
    </legend>
    <input type="date" name="productMadeDate" class="form-control" value="${element.madeDate}" required>
</div>
<div class="form-group">
    <legend>
        <h6>
            Description
        </h6>
    </legend>
    <textarea name="productDescription" class="form-control-static" required>${element.description}</textarea>
</div>
<div class="form-group">
    <legend>
        <h6>
            Ua Description
        </h6>
    </legend>
    <textarea name="productUaDescription" class="form-control-static" required>${descriptionsTranslations[0]}</textarea>
</div>
<div class="form-group">
    <legend>
        <h6>
            En Description
        </h6>
    </legend>
    <textarea name="productEnDescription" class="form-control-static" required>${descriptionsTranslations[1]}</textarea>
</div>

