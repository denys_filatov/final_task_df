<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="istore" tagdir="/WEB-INF/tags" %>

<!-- Search form -->
<form name="searchNameForm" class="search" method="post" action="controller">
    <input type="hidden" name="command" value="orderedListProducts"/>
    <div id="findProducts" class="panel panel-success collapse">
        <div class="panel-heading"><fmt:message key="aside_find_products"/></div>
        <div class="panel-body">
            <div class="input-group">
                <input type="text" name="searchName" class="form-control"
                       placeholder="<fmt:message key="aside_search_query"/>" required>
                <div class="input-group-btn">
                    <button class="btn btn-outline-secondary" type="submit"><fmt:message key="aside_go"/>
                </div>
            </div>
            <br>
        </div>
    </div>
</form>
<!-- /Search form -->

<script>
    function validatePrice(){
        var from = document.forms["priceForm"]["priceFrom"].value;
        var to = document.forms["priceForm"]["priceTo"].value;
        if (from > to){
            alert("Price 'from' should be less than 'to' ");
            return false;
        }
    }
</script>

<form name="priceForm" onsubmit="return validatePrice()" class="filter" method="post" action="controller">
    <input type="hidden" name="command" value="orderedListProducts"/>
    <div id="filtersCatalog" class="panel panel-success collapse">
        <div class="panel-heading"><fmt:message key="aside_price"/></div>
        <div class="form-inline">
            <c:choose>
                <c:when test="${priceFrom == null}">
                    <input type="number" name="priceFrom" class="form-control " placeholder="$0" required>
                </c:when>
                <c:otherwise>
                    <input type="number" name="priceFrom"  class="form-control " placeholder="${priceFrom}">
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${priceTo == null}">
                    <input type="number" name="priceTo" class="form-control " placeholder="$10000" required>
                </c:when>
                <c:otherwise>
                    <input type="number" name="priceTo" class="form-control " placeholder="${priceTo}">
                </c:otherwise>
            </c:choose>
            <div class="input-group-btn">
                <button class="btn btn-outline-secondary" type="submit"><fmt:message key="aside_go"/>
            </div>
        </div>
        <br>
    </div>
</form>

<!-- Categories -->
<form>
    <div id="productCatalog" class="panel panel-success collapse">
        <div class="panel-heading"><fmt:message key="aside_product_catalog"/></div>
        <istore:categories categories="${categories}"></istore:categories>
    </div>
</form>

<!-- /Categories -->
