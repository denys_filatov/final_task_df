<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="form-group">
    <input type="text" readonly="readonly" class="form-control" value=${element.account}>
</div>
<div class="form-group">
    <input type="url" readonly="readonly" class="form-control" value=${element.created}>
</div>

<div class="form-group">
    <select name="status">
        <c:if test="${element.orderStatus == 'Pending'}">
            <option value="PENDING" selected="selected">Pending</option>
            <option value="COMPLETED">Completed</option>
            <option value="CANCELLED">Cancelled</option>
        </c:if>
        <c:if test="${element.orderStatus == 'Completed'}">
            <option value="PENDING" selected="selected">Pending</option>
            <option value="COMPLETED" selected="selected">Completed</option>
            <option value="CANCELLED">Cancelled</option>
        </c:if>
        <c:if test="${element.orderStatus == 'Cancelled'}">
            <option value="PENDING" selected="selected">Pending</option>
            <option value="COMPLETED">Completed</option>
            <option value="CANCELLED" selected="selected">Cancelled</option>
        </c:if>
    </select>
</div>
