<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" trimDirectiveWhitespaces="true" %>

<form>
    <div id="productCatalog" class="panel panel-success collapse">
        <div class="panel-heading">Редактор</div>
        <div class="list-group">
            <a href="controller?command=redactor&type=user" class="list-group-item">Користувачі</a>
            <a href="controller?command=redactor&type=category" class="list-group-item">Категорії</a>
            <a href="controller?command=redactor&type=producer" class="list-group-item">Виробники</a>
            <a href="controller?command=redactor&type=product" class="list-group-item">Товари</a>
            <a href="controller?command=redactor&type=ordering" class="list-group-item">Замовлення</a>
        </div>
    </div>
</form>
