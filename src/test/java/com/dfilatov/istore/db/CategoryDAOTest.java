package com.dfilatov.istore.db;

import com.dfilatov.istore.db.entity.Category;
import com.dfilatov.istore.db.entity.Product;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CategoryDAOTest {

    Category category;
    CategoryDAO categoryDAO;

    @Before
    public void setUp() throws Exception {
        category = createAndInsertCategory();
        categoryDAO = new CategoryDAO();
    }

    @After
    public void tearDown() throws Exception {
        categoryDAO.deleteCategoryTranslationsById(category.getId());
        categoryDAO.deleteElementById(category.getId());
    }

    @Test
    public void findCategory() {
        List<Category> result = categoryDAO.findCategory("en");
        Assert.assertTrue(result.stream().anyMatch(x -> x.getName().equals("test")));
    }

    private Category createAndInsertCategory() {
        Category category = new Category();
        category.setName("test");
        category.setCount(4);
        CategoryDAO categoryDAO = new CategoryDAO();
        categoryDAO.insertElement(category);
        int id = categoryDAO.findCategoryIdByName("test");
        category.setId(id);
        List<String> translation = new ArrayList<>();
        translation.add("тест");
        translation.add("test");
        categoryDAO.insertCategoryTranslations(id, translation);
        return category;
    }

    @Test
    public void findCategoryTranslationsById() {
        List<String> translations = categoryDAO.findCategoryTranslationsById((long) category.getId());
        Assert.assertFalse(translations.isEmpty());
        Assert.assertEquals(2, translations.size());
        Assert.assertEquals("test", translations.get(1));
    }

    @Test
    public void getAllElements() {
        List<Category> categories = categoryDAO.getAllElements();
        Assert.assertFalse(categories.isEmpty());
        Assert.assertTrue(categories.stream().anyMatch(x -> x.getName().equals("test")));
    }

    @Test
    public void getAllElementsWithoutLocalization() {
        List<Category> categories = categoryDAO.getAllElementsWithoutLocalization();
        Assert.assertFalse(categories.isEmpty());
        Assert.assertTrue(categories.stream().anyMatch(x -> x.getName().equals("test")));
    }

    @Test
    public void findElementById() {
        Assert.assertEquals(category, categoryDAO.findElementById(category.getId()));
    }

    @Test
    public void deleteElementById() {
        categoryDAO.deleteCategoryTranslationsById(category.getId());
        categoryDAO.deleteElementById(category.getId());
        Assert.assertEquals(-1, categoryDAO.findCategoryIdByName(category.getName()));
    }

    @Test
    public void deleteCategoryTranslationsById() {
        Assert.assertEquals(2, categoryDAO.findCategoryTranslationsById((long) category.getId()).size());
        categoryDAO.deleteCategoryTranslationsById(category.getId());
        Assert.assertEquals(new ArrayList<>(), categoryDAO.findCategoryTranslationsById((long) category.getId()));
    }

    @Test
    public void findCategoryIdByName() {
        int id = categoryDAO.findCategoryIdByName(category.getName());
        Assert.assertNotEquals(-1, id);
    }

    @Test
    public void updateElement() {
        Category category1 = categoryDAO.findElementById(category.getId());
        category1.setName("test1");
        categoryDAO.updateElement(category1);
        int id = categoryDAO.findCategoryIdByName("test1");
        Assert.assertNotEquals(-1, id);
    }

    @Test
    public void insertCategoryTranslations() {
        Assert.assertNotEquals(new ArrayList<>(), categoryDAO.findCategoryTranslationsById((long) category.getId()));
    }

    @Test
    public void insertElement() {
        Assert.assertEquals(category, categoryDAO.findElementById(category.getId()));
    }

    @Test
    public void updateElementTranslations() {
        List<String> newTranslations = new ArrayList<>();
        newTranslations.add("test1");
        newTranslations.add("тест1");
        categoryDAO.updateElementTranslations((long) category.getId(), newTranslations);
        List<String> result = categoryDAO.findCategoryTranslationsById((long) category.getId());
        Assert.assertEquals(result, newTranslations);
    }

    @Test
    public void updateSingleTranslation() {
        String enTranslation = "test3";
        categoryDAO.updateSingleTranslation((long) category.getId(), enTranslation, 2);
        List<String> result = categoryDAO.findCategoryTranslationsById((long) category.getId());
        Assert.assertTrue(result.contains(enTranslation));
    }

    @Test
    public void updateElementsCount() {
        List<Category> categories = categoryDAO.getAllElements();
        Product product = new Product();
        product.setName("test");
        product.setDescription("test");
        product.setImageLink("/test");
        product.setPrice(BigDecimal.valueOf(1000L));
        product.setProducer("Apple");
        product.setCategory("test");
        product.setMadeDate(new Date(System.currentTimeMillis()));
        product.setCount(2);
        ProductsDAO productsDAO = new ProductsDAO();
        productsDAO.insertElement(product);
        product.setId((long) productsDAO.findElementIdByName("test"));
        categoryDAO.updateElementsCount(categories);
        Category category1 = categoryDAO.findElementById(category.getId());
        Assert.assertEquals(2, category1.getCount());
        productsDAO.deleteElementById(product.getId());
    }

    @Test
    public void updateSingleElementCount() {
        Product product = new Product();
        product.setName("test2");
        product.setDescription("test2");
        product.setImageLink("/test");
        product.setPrice(BigDecimal.valueOf(1000L));
        product.setProducer("Apple");
        product.setCategory("test");
        product.setMadeDate(new Date(System.currentTimeMillis()));
        product.setCount(3);
        ProductsDAO productsDAO = new ProductsDAO();
        productsDAO.insertElement(product);
        product.setId((long) productsDAO.findElementIdByName("test2"));
        categoryDAO.updateSingleElementCount(category);
        Category category1 = categoryDAO.findElementById(category.getId());
        Assert.assertEquals(3, category1.getCount());
        productsDAO.deleteElementById(product.getId());
    }
}