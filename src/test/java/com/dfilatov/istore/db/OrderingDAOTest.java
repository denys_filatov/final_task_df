package com.dfilatov.istore.db;

import com.dfilatov.istore.db.entity.Account;
import com.dfilatov.istore.db.entity.Ordering;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class OrderingDAOTest {

    OrderingDAO orderingDAO;
    Ordering ordering;
    UserDAO userDAO;
    Account account;

    @Before
    public void setUp() throws Exception {
        orderingDAO = new OrderingDAO();
        userDAO = new UserDAO();
        ordering = new Ordering();
        ordering.setOrderStatus(OrderStatus.PENDING.name());
        account = userDAO.findUserByLogin("TestUser");
        ordering.setAccount(account.getName());
        ordering.setCreated(new Timestamp(System.currentTimeMillis()));
        orderingDAO.insertElement(ordering);
        ordering.setId(orderingDAO.getLastInsertedId());
    }

    @After
    public void tearDown() throws Exception {
        orderingDAO.deleteElementById(ordering.getId());
    }

    @Test
    public void findElementById() {
        Ordering ordering2 = orderingDAO.findElementById(ordering.getId());
        Assert.assertEquals(ordering.getAccount(), ordering2.getAccount());
    }

    @Test
    public void getAllElements() {
        List<Ordering> orderings = orderingDAO.getAllElements();
        Assert.assertFalse(orderings.isEmpty());
    }

    @Test
    public void deleteElementById() {
        orderingDAO.deleteElementById(ordering.getId());
        Assert.assertNull(orderingDAO.findElementById(ordering.getId()));
    }

    @Test
    public void updateElement() {
        ordering.setOrderStatus(OrderStatus.COMPLETED.name());
        orderingDAO.updateElement(ordering);
        Ordering ordering1 = orderingDAO.findElementById(ordering.getId());
        Assert.assertEquals(OrderStatus.COMPLETED.name(), ordering1.getOrderStatus());
    }

    @Test
    public void insertElement() {
        Assert.assertNotNull(orderingDAO.findElementById(ordering.getId()));
    }

    @Test
    public void getLastInsertedId() {
        Assert.assertEquals(java.util.Optional.of(orderingDAO.getLastInsertedId()).get(), ordering.getId());
    }

    @Test
    public void insertOrderItems() {
        Map<Long, Integer> productAndCount = new LinkedHashMap<>();
        productAndCount.put(3L,1);
        productAndCount.put(4L,1);
        orderingDAO.insertOrderItems(ordering.getId(), productAndCount);
        Map<Long, Integer> result = orderingDAO.findProductAndCountFromOrderByOrderId(ordering.getId());
        Assert.assertEquals(2, result.size());
        orderingDAO.deleteOrderItemsByOrderId(ordering.getId());
    }

    @Test
    public void completeOrder() {
        orderingDAO.completeOrder(ordering);
        Ordering ordering1 = orderingDAO.findElementById(ordering.getId());
        Assert.assertEquals(OrderStatus.COMPLETED.name(), ordering1.getOrderStatus());
    }

    @Test
    public void cancelOrder() {
        orderingDAO.cancelOrder(ordering);
        Ordering ordering1 = orderingDAO.findElementById(ordering.getId());
        Assert.assertEquals(OrderStatus.CANCELED.name(), ordering1.getOrderStatus());
    }

    @Test
    public void findProductAndCountFromOrderByOrderId() {
        Map<Long, Integer> productAndCount = new LinkedHashMap<>();
        productAndCount.put(3L,1);
        orderingDAO.insertOrderItems(ordering.getId(), productAndCount);
        Map<Long, Integer> result = orderingDAO.findProductAndCountFromOrderByOrderId(ordering.getId());
        Assert.assertEquals(1, result.size());
        orderingDAO.deleteOrderItemsByOrderId(ordering.getId());
    }

    @Test
    public void findOrdersIdByUserId() {
        Assert.assertNotEquals(0, orderingDAO.findOrdersIdByUserId(account.getId()).size());
    }

    @Test
    public void deleteOrderItemsByOrderId() {
        Map<Long, Integer> productAndCount = new LinkedHashMap<>();
        productAndCount.put(3L,1);
        productAndCount.put(4L,1);
        orderingDAO.insertOrderItems(ordering.getId(), productAndCount);
        Map<Long, Integer> result = orderingDAO.findProductAndCountFromOrderByOrderId(ordering.getId());
        Assert.assertEquals(2, result.size());
        orderingDAO.deleteOrderItemsByOrderId(ordering.getId());
        result = orderingDAO.findProductAndCountFromOrderByOrderId(ordering.getId());
        Assert.assertEquals(0, result.size());
    }
}