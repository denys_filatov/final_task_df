package com.dfilatov.istore.db;

import com.dfilatov.istore.db.entity.Account;
import com.dfilatov.istore.db.entity.Ordering;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.List;

public class UserDAOTest {

    private UserDAO userDAO;
    private Account account;

    @Before
    public void setUp() throws Exception {
        userDAO = new UserDAO();
        account = new Account();
        account.setName("testAccount");
        account.setRole(AccountRole.USER.getName());
        account.setFirstName("test");
        account.setLastName("test");
        account.setBlocked(false);
        account.setPassword("test");
        userDAO.insertElement(account);
        account.setId(userDAO.findUserByLogin(account.getName()).getId());
    }

    @After
    public void tearDown() throws Exception {
        userDAO.deleteElementById(account.getId());
    }

    @Test
    public void findElementById() {
        Assert.assertEquals(account, userDAO.findElementById(account.getId()));
    }

    @Test
    public void findUserByLogin() {
        Assert.assertEquals(account, userDAO.findUserByLogin(account.getName()));
    }

    @Test
    public void findUserOrdersCountByID() {
        OrderingDAO orderingDAO = new OrderingDAO();
        Ordering ordering = new Ordering();
        ordering.setAccount(account.getName());
        ordering.setOrderStatus(OrderStatus.PENDING.name());
        ordering.setCreated(new Timestamp(System.currentTimeMillis()));
        orderingDAO.insertElement(ordering);
        Assert.assertEquals(1, userDAO.findUserOrdersCountByID((long) account.getId()));
        orderingDAO.findOrdersIdByUserId(account.getId()).forEach(orderingDAO::deleteElementById);
    }

    @Test
    public void findAccountRoleIdByName() {
        Assert.assertNotEquals(-1, userDAO.findAccountRoleIdByName(account.getRole()));
    }

    @Test
    public void findAllRoles() {
        List<String> roles = userDAO.findAllRoles();
        Assert.assertTrue(roles.contains(AccountRole.ADMIN.getName()));
        Assert.assertTrue(roles.contains(AccountRole.USER.getName()));
    }

    @Test
    public void getAllElements() {
        List<Account> users = userDAO.getAllElements();
        Assert.assertFalse(users.isEmpty());
        Assert.assertTrue(users.stream().anyMatch(u -> u.getName().equals(account.getName())));
    }

    @Test
    public void deleteElementById() {
        userDAO.deleteElementById(account.getId());
        Assert.assertNull(userDAO.findElementById(account.getId()));
    }

    @Test
    public void updateElement() {
        account.setFirstName("TestChange");
        userDAO.updateElement(account);
        Account testAccount = userDAO.findElementById(account.getId());
        Assert.assertEquals("TestChange", testAccount.getFirstName());
    }

    @Test
    public void insertElement() {
        Assert.assertEquals(account, userDAO.findElementById(account.getId()));
    }
}