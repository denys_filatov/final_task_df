package com.dfilatov.istore.db;

import com.dfilatov.istore.db.entity.Product;
import org.junit.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProductsDAOTest {

    ProductsDAO productsDAO;
    Product product;
    

    @Before
    public void setUp() throws Exception {
        productsDAO = new ProductsDAO();
        product = new Product();
        product.setName("test");
        product.setDescription("test");
        product.setImageLink("/test");
        product.setPrice(BigDecimal.valueOf(1000L));
        product.setProducer("Apple");
        product.setCategory("desktop");
        product.setMadeDate(new Date(System.currentTimeMillis()));
        product.setCount(5);
        productsDAO.insertElement(product);
        product.setId((long) productsDAO.findElementIdByName(product.getName()));
        List<String> nameTranslations = new ArrayList<>();
        nameTranslations.add("тест");
        nameTranslations.add("test");
        List<String> descriptionTranslations = new ArrayList<>();
        descriptionTranslations.add("тест");
        descriptionTranslations.add("test");
        productsDAO.insertTranslationsByProductId(Math.toIntExact(product.getId()), nameTranslations, descriptionTranslations);
    }

    @After
    public void tearDown() throws Exception {
        productsDAO.deleteProductTranslationsById(product.getId());
        productsDAO.deleteElementById(product.getId());
    }

    @Test
    public void findProducts() {
        List<Product> productList = productsDAO.findProducts("ua", 0, 5);
        Assert.assertEquals(5, productList.size());
    }

    @Test
    public void testFindProducts() {
        String query = "order by name";
        List<Product> productList = productsDAO.findFilterProducts("en", query, 0, 5);
        Assert.assertTrue(productList.get(0).getName().compareTo(productList.get(1).getName()) < 0);
    }

    @Test
    public void getAllElements() {
        List<Product> products = productsDAO.getAllElements();
        Assert.assertFalse(products.isEmpty());
        Assert.assertTrue(products.stream().anyMatch(p -> p.getName().equals(product.getName())));
    }

    @Test
    public void findElementById() {
        Product product1 = productsDAO.findElementById(product.getId());
        Assert.assertEquals(product.getName(), product1.getName());
    }

    @Test
    public void deleteElementById() {
        productsDAO.deleteProductTranslationsById(product.getId());
        productsDAO.deleteElementById(product.getId());
        Product product1 = productsDAO.findElementById(product.getId());
        Assert.assertNull(product1);
    }

    @Test
    public void deleteProductTranslationsById() {
        productsDAO.deleteProductTranslationsById(product.getId());
        List<String> translations = productsDAO.findProductsDescriptionsTranslationsById(product.getId());
        Assert.assertEquals(0, translations.size());
    }

    @Test
    public void findCategoryIdByName() {
        Assert.assertNotEquals(-1, productsDAO.findCategoryIdByName(product.getCategory()));
    }

    @Test
    public void findProducerIdByName() {
        Assert.assertNotEquals(-1, productsDAO.findProducerIdByName(product.getProducer()));
    }

    @Test
    public void updateElement() {
        product.setName("test2");
        productsDAO.updateElement(product);
        Assert.assertNotEquals(-1, productsDAO.findElementIdByName(product.getName()));
    }

    @Test
    public void updateElementNameTranslations() {
        List<String> translations = new ArrayList<>();
        translations.add("тест2");
        translations.add("test2");
        productsDAO.updateElementNameTranslations(product.getId(), translations);
        List<String> result = productsDAO.findProductsNameTranslationsById(product.getId());
        Assert.assertEquals(translations, result);
    }

    @Test
    public void updateSingleNameTranslation() {
        String nameEn = "test3";
        productsDAO.updateSingleNameTranslation(product.getId(), nameEn, 2);
        List<String> result = productsDAO.findProductsNameTranslationsById(product.getId());
        Assert.assertTrue(result.contains(nameEn));
    }

    @Test
    public void updateElementDescriptionTranslations() {
        List<String> translations = new ArrayList<>();
        translations.add("тест4");
        translations.add("test4");
        productsDAO.updateElementDescriptionTranslations(product.getId(), translations);
        List<String> result = productsDAO.findProductsDescriptionsTranslationsById(product.getId());
        Assert.assertEquals(translations, result);
    }

    @Test
    public void updateSingleDescriptionTranslation() {
        String nameEn = "test5";
        productsDAO.updateSingleDescriptionTranslation(product.getId(), nameEn, 2);
        List<String> result = productsDAO.findProductsDescriptionsTranslationsById(product.getId());
        Assert.assertTrue(result.contains(nameEn));
    }

    @Test
    public void findProductsNameTranslationsById() {
        List<String> translations = productsDAO.findProductsNameTranslationsById(product.getId());
        Assert.assertFalse(translations.isEmpty());
    }

    @Test
    public void findProductsDescriptionsTranslationsById() {
        List<String> translations = productsDAO.findProductsDescriptionsTranslationsById(product.getId());
        Assert.assertFalse(translations.isEmpty());
    }

    @Test
    public void findElementIdByName() {
        Assert.assertNotEquals(-1, productsDAO.findElementIdByName(product.getName()));
    }

    @Test
    public void insertElement() {
        Assert.assertNotNull(productsDAO.findElementById(product.getId()));
    }

    @Test
    public void insertTranslationsByProductId() {
        Assert.assertNotEquals(0, productsDAO.findProductsNameTranslationsById(product.getId()).size());
    }

    @Test
    public void insertSingleTranslationByProductId() {
        Assert.assertNotEquals(0, productsDAO.findProductsDescriptionsTranslationsById(product.getId()).size());
    }

    @Test
    public void getNoOfRecords() {
        productsDAO.findProducts("ua",0, 5);
        Assert.assertNotEquals(0, productsDAO.getNoOfRecords());
    }
}