package com.dfilatov.istore.db;

import com.dfilatov.istore.db.entity.Producer;
import com.dfilatov.istore.db.entity.Product;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class ProducerDAOTest {

    Producer producer;
    ProducerDAO producerDAO;
    @Before
    public void setUp() throws Exception {
        producer = new Producer();
        producerDAO = new ProducerDAO();
        producer.setName("test");
        producerDAO.insertElement(producer);
        List<Producer> producerList = producerDAO.getAllElements();
        producer = producerList.stream().filter(p->p.getName().equals(producer.getName())).findFirst().get();
    }

    @After
    public void tearDown() throws Exception {
        producerDAO.deleteElementById(producer.getId());
    }

    @Test
    public void getAllElements() {
        List<Producer> producers = producerDAO.getAllElements();
        Assert.assertFalse(producers.isEmpty());
    }

    @Test
    public void findCountOfProducerProducts() {
        ProductsDAO productsDAO = new ProductsDAO();
        Product product = new Product();
        product.setCount(2);
        product.setMadeDate(new Date(System.currentTimeMillis()));
        product.setCategory("desktop");
        product.setProducer("test");
        product.setPrice(BigDecimal.valueOf(1000L));
        product.setImageLink("/test");
        product.setDescription("test");
        product.setName("test");
        productsDAO.insertElement(product);
        product.setId((long) productsDAO.findElementIdByName(product.getName()));
        int count = producerDAO.findCountOfProducerProducts(producer.getId());
        Assert.assertEquals(2, count);
        productsDAO.deleteElementById(product.getId());
    }

    @Test
    public void findElementById() {
        Producer testProducer = producerDAO.findElementById(producer.getId());
        Assert.assertEquals(producer, testProducer);
    }

    @Test
    public void deleteElementById() {
        producerDAO.deleteElementById(producer.getId());
        Assert.assertNull(producerDAO.findElementById(producer.getId()));
    }

    @Test
    public void updateElement() {
        producer.setName("testName");
        producerDAO.updateElement(producer);
        Producer producerTest = producerDAO.findElementById(producer.getId());
        Assert.assertEquals("testName", producerTest.getName());
    }

    @Test
    public void insertElement() {
        Producer producer1 = producerDAO.findElementById(producer.getId());
        Assert.assertEquals(producer, producer1);
    }

    @Test
    public void updateElementsCount() {
        ProductsDAO productsDAO = new ProductsDAO();
        Product product = new Product();
        product.setCount(2);
        product.setMadeDate(new Date(System.currentTimeMillis()));
        product.setCategory("desktop");
        product.setProducer("test");
        product.setPrice(BigDecimal.valueOf(1000L));
        product.setImageLink("/test");
        product.setDescription("test");
        product.setName("test");
        productsDAO.insertElement(product);
        product.setId((long) productsDAO.findElementIdByName(product.getName()));
        List<Producer> producers = producerDAO.getAllElements();
        producerDAO.updateElementsCount(producers);
        int count = producerDAO.findElementById(producer.getId()).getCount();
        Assert.assertEquals(2, count);
        productsDAO.deleteElementById(product.getId());
    }

    @Test
    public void updateSingleElementCount() {
        ProductsDAO productsDAO = new ProductsDAO();
        Product product = new Product();
        product.setCount(2);
        product.setMadeDate(new Date(System.currentTimeMillis()));
        product.setCategory("desktop");
        product.setProducer("test");
        product.setPrice(BigDecimal.valueOf(1000L));
        product.setImageLink("/test");
        product.setDescription("test");
        product.setName("test");
        productsDAO.insertElement(product);
        product.setId((long) productsDAO.findElementIdByName(product.getName()));
        producerDAO.updateSingleElementCount(producer);
        int count = producerDAO.findElementById(producer.getId()).getCount();
        Assert.assertEquals(2, count);
        productsDAO.deleteElementById(product.getId());
    }
}