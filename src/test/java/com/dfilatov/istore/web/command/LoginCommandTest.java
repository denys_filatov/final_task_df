package com.dfilatov.istore.web.command;

import com.dfilatov.istore.PathConstants;
import org.testng.annotations.Test;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;

import static org.mockito.Mockito.*;

public class LoginCommandTest {

    @Test
    public void testExecute() throws IOException, ServletException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpSession session = mock(HttpSession.class);

        when(request.getSession()).thenReturn(session);
        when(session.getAttribute(PathConstants.DEFAULT_LOCALE)).thenReturn("ua");
        when(request.getParameter("login")).thenReturn("user");
        when(request.getParameter("password")).thenReturn("user");

        new LoginCommand().execute(request, response);

        verify(request, atLeast(1)).getSession();
    }
}