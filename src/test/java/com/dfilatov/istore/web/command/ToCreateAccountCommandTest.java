package com.dfilatov.istore.web.command;

import org.testng.annotations.Test;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static org.mockito.Mockito.*;

public class ToCreateAccountCommandTest {

    @Test
    public void testExecute() throws IOException, ServletException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);

        new ToCreateAccountCommand().execute(request, response);

        verify(request, never()).getSession();
    }
}