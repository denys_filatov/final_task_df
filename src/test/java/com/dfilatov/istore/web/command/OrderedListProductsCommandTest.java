package com.dfilatov.istore.web.command;

import com.dfilatov.istore.PathConstants;
import org.testng.annotations.Test;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;

import static org.mockito.Mockito.*;

public class OrderedListProductsCommandTest {

    @Test
    public void testExecute() throws IOException, ServletException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpSession session = mock(HttpSession.class);

        when(request.getParameter("priceFrom")).thenReturn("400");
        when(request.getParameter("priceTo")).thenReturn("600");
        when(request.getParameter("searchName")).thenReturn("Idea");
        when(request.getParameter("categoryName")).thenReturn("desktop");
        when(request.getParameter("orderQuery")).thenReturn("order by name");
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute(PathConstants.DEFAULT_LOCALE)).thenReturn("ua");

        new OrderedListProductsCommand().execute(request, response);

        verify(request).setAttribute("currentCommand", "orderedListProducts");
    }
}