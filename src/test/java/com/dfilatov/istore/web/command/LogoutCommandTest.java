package com.dfilatov.istore.web.command;

import com.dfilatov.istore.db.AccountRole;
import com.dfilatov.istore.db.entity.Account;
import org.testng.annotations.Test;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;

import static org.mockito.Mockito.*;

public class LogoutCommandTest {

    @Test
    public void testExecute() throws IOException, ServletException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpSession session = mock(HttpSession.class);
        Account account = new Account();
        account.setId(7);
        account.setFirstName("test");
        account.setLastName("test");
        account.setName("test");
        account.setPassword("test");
        account.setBlocked(false);
        account.setRole(AccountRole.USER.getName());

        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("account")).thenReturn(account);

        new LogoutCommand().execute(request, response);

        verify(session).setAttribute("account", null);
    }
}