package com.dfilatov.istore.web.command;

import com.dfilatov.istore.PathConstants;
import org.testng.annotations.Test;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class NoCommandTest {

    @Test
    public void testExecute() throws IOException, ServletException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);

        new NoCommand().execute(request, response);

        verify(request).setAttribute(PathConstants.ERROR_MESSAGE, "Page not found");
    }
}