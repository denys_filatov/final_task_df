package com.dfilatov.istore.web.command;

import org.testng.annotations.Test;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static org.mockito.Mockito.*;

public class CreateAccountCommandTest {

    @Test
    public void testExecute() throws IOException, ServletException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);

        when(request.getParameter("firstName")).thenReturn("test");
        when(request.getParameter("lastName")).thenReturn("test");
        when(request.getParameter("name")).thenReturn("test");
        when(request.getParameter("password")).thenReturn("test");

        new CreateAccountCommand().execute(request, response);

        verify(request, times(4)).getParameter(any());
    }
}