package com.dfilatov.istore.web.filter;

import com.dfilatov.istore.PathConstants;
import com.dfilatov.istore.web.util.SessionUtil;
import org.testng.annotations.Test;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;

import static org.mockito.Mockito.*;

public class CartFilterTest {

    @Test
    public void testDoFilter() throws IOException, ServletException {
        HttpServletResponse response = mock(HttpServletResponse.class);
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpSession session = mock(HttpSession.class);
        FilterChain filterChain = mock(FilterChain.class);

        when(request.getSession()).thenReturn(session);
        when(session.getAttribute(PathConstants.USER_ROLE)).thenReturn("user");
        when(SessionUtil.isCurrentCartIsCreated(request)).thenReturn(false);

        new CartFilter().doFilter(request, response, filterChain);

        verify(request, atLeast(1)).getSession();
        verify(session, atLeast(1)).getAttribute(PathConstants.USER_ROLE);
    }
}