package com.dfilatov.istore.model;

import com.dfilatov.istore.db.entity.Product;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CartTest {

    private Cart cart;

    @Test
    void addProduct() {
        Product product = new Product();
        createProduct(product, 1);
        cart = new Cart();
        cart.addProduct(product, 1);
        Assert.assertEquals(1, cart.getTotalCount());
        Assert.assertFalse(cart.getItems().isEmpty());
    }

    @Test
    void removeProduct() {
        Product product = new Product();
        createProduct(product, 1);
        Product product1 = new Product();
        createProduct(product1, 2);
        cart = new Cart();
        cart.addProduct(product, 1);
        cart.addProduct(product1, 1);
        Assert.assertEquals(2, cart.getTotalCount());
        cart.removeProduct(1, 1);
        Assert.assertEquals(1, cart.getTotalCount());
    }

    @Test
    void getItems() {
        Product product = new Product();
        createProduct(product, 1);
        cart = new Cart();
        cart.addProduct(product, 1);
        List<CartItem> items = new ArrayList<>(cart.getItems());
        Assert.assertEquals(product, items.get(0).getProduct());
        Assert.assertEquals(1, items.size());
    }

    @Test
    void getTotalCount() {
        Product product = new Product();
        createProduct(product, 1);
        cart = new Cart();
        cart.addProduct(product, 1);
        Assert.assertEquals(1, cart.getTotalCount());
        Product product1 = new Product();
        createProduct(product1, 2);
        cart.addProduct(product1, 1);
        Assert.assertEquals(2, cart.getTotalCount());
    }

    @Test
    void getTotalCost() {
        Product product = new Product();
        createProduct(product, 1);
        cart = new Cart();
        cart.addProduct(product, 1);
        Assert.assertEquals(BigDecimal.valueOf(1000L), cart.getTotalCost());
    }

    private void createProduct(Product product, long id) {
        product.setName("test");
        product.setId(id);
        product.setCount(1);
        product.setMadeDate(new Date(System.currentTimeMillis()));
        product.setCategory("desktop");
        product.setProducer("iphone");
        product.setPrice(BigDecimal.valueOf(1000L));
        product.setImageLink("/test.jpg");
        product.setDescription("test");

    }
}
