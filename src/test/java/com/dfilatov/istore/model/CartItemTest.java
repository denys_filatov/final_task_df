package com.dfilatov.istore.model;

import com.dfilatov.istore.db.entity.Product;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.Assert.*;

public class CartItemTest {
    CartItem cartItem;

    @Test
    public void getProduct() {
        cartItem = new CartItem();
        Product product = new Product();
        createProduct(product, 1);
        cartItem.setProduct(product);
        cartItem.setCount(2);
        Assert.assertEquals(product, cartItem.getProduct());
    }

    @Test
    public void setProduct() {
        cartItem = new CartItem();
        Product product = new Product();
        createProduct(product, 2);
        cartItem.setProduct(product);
        cartItem.setCount(3);
        Assert.assertEquals(product, cartItem.getProduct());
    }

    @Test
    public void getCount() {
        cartItem = new CartItem();
        Product product = new Product();
        createProduct(product, 1);
        cartItem.setProduct(product);
        cartItem.setCount(2);
        assertEquals(2, cartItem.getCount());
    }

    @Test
    public void setCount() {
        cartItem = new CartItem();
        Product product = new Product();
        createProduct(product, 1);
        cartItem.setProduct(product);
        cartItem.setCount(3);
        assertEquals(3, cartItem.getCount());
    }

    private void createProduct(Product product, long id) {
        product.setName("test");
        product.setId(id);
        product.setCount(1);
        product.setMadeDate(new Date(System.currentTimeMillis()));
        product.setCategory("desktop");
        product.setProducer("iphone");
        product.setPrice(BigDecimal.valueOf(1000L));
        product.setImageLink("/test.jpg");
        product.setDescription("test");

    }

}