<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div>
    <nav class="navbar navbar-default">
        <form method="post" action="controller">
            <input type="hidden" name="command" value="orderedListProducts"/>
            <select onchange="javascript:this.form.submit()" name="orderQuery" class="btn btn-primary navbar-btn navbar-right sign-in">>
                <option value=""><fmt:message key="products_sort_command"/></option>
                <option value="order by name"><fmt:message key="products_sort_name_asc"/></option>
                <option value="order by name desc"><fmt:message key="products_sort_name_desc"/></option>
                <option value="order by price"><fmt:message key="products_sort_price_asc"/></option>
                <option value="order by price desc"><fmt:message key="products_sort_price_desc"/></option>
                <option value="order by date"><fmt:message key="products_sort_date_asc"/></option>
                <option value="order by date desc"><fmt:message key="products_sort_date_desc"/></option>
            </select>
        </form>
    </nav>
</div>
<div>
</div>
<div id="productList">
    <jsp:include page="../fragment/product-list.jsp"/>
</div>