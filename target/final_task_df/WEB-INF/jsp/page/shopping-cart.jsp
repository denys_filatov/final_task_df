<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="cart" uri="https://tag.CartAmountTag" %>
<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" trimDirectiveWhitespaces="true" %>
<!DOCTYPE html>
<html>
<head>
    <title><fmt:message key="cart_title"/></title>
    <link href="/static/css/bootstrap.css" rel="stylesheet">
    <link href="/static/css/bootstrap-theme.css" rel="stylesheet">
    <link href="/static/css/font-awesome.css" rel="stylesheet">
    <link href="/static/css/app.css" rel="stylesheet">
</head>
<body>
<header>
    <jsp:include page="../fragment/header.jsp"/>
</header>
<div class="container-fluid">
    <div class="row">
        <main>
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th class="text-center" scope="col"><fmt:message key="cart_product"/></th>
                    <th class="text-center" scope="col"><fmt:message key="cart_count"/></th>
                    <th class="text-center" scope="col"><fmt:message key="cart_actions"/></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="item" items="${CURRENT_CART.items}">
                    <tr>
                        <td class="text-center">${item.product.name}</td>
                        <td class="text-center">${item.count}</td>
                        <td class="text-center">
                            <div class="btn-group">
                                <form method="post" action="controller">
                                    <input type="hidden" name="command" value="deleteFromCart">
                                    <input type="hidden" name="id" value="${item.product.id}">
                                    <button class="btn btn-primary" type="submit"><fmt:message key="cart_delete"/></button>
                                </form>
                            </div>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
                <tfoot>
                <tr>
                    <cart:cartCost cart="${CURRENT_CART}"/>
                    <c:if test="${CURRENT_CART.totalCount>0}">
                        <th class="text-center" scope="col">
                            <a class="btn btn-success" href="controller?command=makeOrder"><fmt:message key="cart_confirm"/></a>
                        </th>
                    </c:if>
                </tr>
                </tfoot>
            </table>
        </main>
    </div>
</div>
<footer class="footer">
    <jsp:include page="../fragment/footer.jsp"/>
</footer>
</body>
</html>
