<input type="hidden" name="type" value="category"/>
<div class="form-group">
    <legend>
        <h6>
            NAME
        </h6>
    </legend>
    <input type="text" name="categoryName" class="form-control" value="${element.name}" required>
</div>
<div class="form-group">
    <legend class="the-legend">
        <h6>
            UA
        </h6>
    </legend>
    <input type="text" name="categoryUaTranslation" class="form-control" value="${translations[0]}" required>
</div>
<div class="form-group">
    <legend class="the-legend">
        <h6>
            EN
        </h6>
    </legend>
    <input type="text" name="categoryEnTranslation" class="form-control" value="${translations[1]}" required>
</div>


