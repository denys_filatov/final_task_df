<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" trimDirectiveWhitespaces="true" %>

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <c:if test="${account.role != 'admin'}">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><fmt:message key="header_store_name"/> </a>
            </c:if>
        </div>
        <div class="collapse navbar-collapse">
            <c:choose>
                <c:when test="${account == null}">
                    <form id="to_login_form" action="controller">
                        <input type="hidden" name="command" value="toLogin"/>
                        <input type="submit" class="btn btn-primary navbar-btn navbar-right sign-in"
                               value='<fmt:message key="header_sign_in"/>'>
                    </form>
                </c:when>
                <c:otherwise>
                    <form id="to_login_form" action="controller">
                        <input type="hidden" name="command" value="Logout"/>
                        <input type="submit" class="btn btn-primary navbar-btn navbar-right sign-in"
                               value='<fmt:message key="header_sign_out"/>'>
                    </form>
                    <c:if test="${userRole.name == 'user'}">
                        <form id="to_personal_account" action="controller">
                            <input type="hidden" name="command" value="toAccount"/>
                            <input type="submit" class="btn btn-primary navbar-btn navbar-right sign-in"
                                   value='<fmt:message key="header_account"/>'>
                        </form>
                    </c:if>
                </c:otherwise>
            </c:choose>
            <c:if test="${account.role != 'admin'}">
                <form method="post" action="controller">
                    <input type="hidden" name="command" value="changeLanguage"/>
                    <select onchange="javascript:this.form.submit()" name="localeToSet"
                            class="btn btn-primary navbar-btn navbar-right sign-in">>
                        <c:choose>
                            <c:when test="${not empty defaultLocale}">
                                <option value="">${defaultLocale}</option>
                            </c:when>
                            <c:otherwise>
                                <option value=""/>
                            </c:otherwise>
                        </c:choose>

                        <c:forEach var="localeName" items="${locales}">
                            <c:if test="${localeName != defaultLocale }">
                                <option value="${localeName}">${localeName}</option>
                            </c:if>
                        </c:forEach>
                    </select>
                </form>
                <form method="post" action="controller">
                    <input type="hidden" name="command" value="showCart"/>
                    <input type="submit" class="btn btn-success navbar-btn navbar-right sign-in" value=<fmt:message
                            key="header_cart"/>(${CURRENT_CART.totalCount})>
                </form>
            </c:if>
        </div>
    </div>
</nav>