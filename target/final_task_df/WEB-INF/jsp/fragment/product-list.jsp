<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<div class="row">
    <c:forEach var="p" items="${products}">
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3 col-xlg-2">
            <!-- PRODUCT DATA -->
            <div id="1" class="panel panel-default product">
                <div class="panel-body">
                    <form method="post" action="controller">
                        <input type="hidden" name="command" value="addProductToCard"/>
                        <input type="hidden" name="page" value="${page}"/>
                        <input type="hidden" name="query" value="${query}"/>
                        <input type="hidden" name="id" value="${p.id}"/>
                        <div class="thumbnail">
                            <img src="/media/${p.imageLink}" alt="${p.name}">
                            <div class="desc">
                                <div class="cell">
                                    <p>
                                        <span class="title"><fmt:message
                                                key="product_list_details"/></span>${p.description}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <h4 class="name">${p.name}</h4>
                        <div class="price">$ ${p.price}</div>
                        <div class="list-group">
                            <span class="list-group-item"> <small><fmt:message
                                    key="product_list_category"/>:</small> <span
                                    class="category">${p.category}</span></span>
                            <span class="list-group-item"> <small><fmt:message
                                    key="product_list_producer"/>:</small><span
                                    class="producer">${p.producer}</span></span>
                            <span class="list-group-item"> <small><fmt:message key="product_list_date"/>:</small> <span
                                    class="made_date"><fmt:formatDate value="${p.madeDate}" pattern="dd/MM/yy"/></span></span>
                            <span class="list-group-item"> <small><fmt:message
                                    key="product_list_available"/>:</small><span
                                    class="count">${p.count-p.reserved}</span></span>
                            <input type="submit" class="btn btn-primary center-block"
                                   value="<fmt:message key="product_list_buy"/>">
                        </div>
                    </form>
                </div>
            </div>
            <!-- /PRODUCT DATA -->
        </div>
    </c:forEach>
</div>
<div class="text-center">
    <ul class="pagination">
        <c:if test="${page != 1}">
            <li><a href="controller?command=${currentCommand}&page=${page - 1}${query}">Previous</a></li>
        </c:if>
        <c:forEach begin="1" end="${noOfPages}" var="i">
            <c:choose>
                <c:when test="${page eq i}">
                    <li><a>${i}</a></li>
                </c:when>
                <c:otherwise>
                    <li><a href="controller?command=${currentCommand}&page=${i}${query}">${i}</a></li>
                </c:otherwise>
            </c:choose>
        </c:forEach>
        <c:if test="${page lt noOfPages}">
            <li><a href="/controller?command=${currentCommand}&page=${page+1}${query}">Next</a></li>
        </c:if>
    </ul>
</div>