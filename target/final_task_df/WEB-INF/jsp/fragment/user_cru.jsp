<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<input type="hidden" name="type" value="user"/>
<div class="form-group">
    <legend>NickName</legend>
    <input type="text" name="name" class="form-control" value="${element.name}" required>
</div>
<div class="form-group">
    <legend>Password</legend>
    <input type="password" name="password" class="form-control" value="${element.password}" required>
</div>
<div class="form-group">
    <legend>First name</legend>
    <input type="text" name="firstName" class="form-control" value="${element.firstName}" required>
</div>
<div class="form-group">
    <legend>Last name</legend>
    <input type="text" name="lastName" class="form-control" value="${element.lastName}" required>
</div>

<div class="form-group">
    <legend>Role</legend>
    <select name="role" class="form-control">
        <option selected="selected">${element.role}</option>
        <c:forEach var="role" items="${roles}">
            <c:if test="${role != element.role}">
                <option>${role}</option>
            </c:if>
        </c:forEach>
    </select>
</div>
<div class="form-group">
    <legend>Status</legend>
    <c:if test="${element.blocked}">
        <label class="form-control"><input type="checkbox" name="blocked" value="unblocked" class="form-control">Unblock</label>
    </c:if>
    <c:if test="${!element.blocked}">
        <label><input type="checkbox" name="blocked" value="blocked" class="form-control">Block</label>
    </c:if>
</div>
